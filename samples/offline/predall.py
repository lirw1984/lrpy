#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
pred app for sched_str.

@author: lirw (lirw@ynao.ac.cn)
"""

import os
import sys
import re
from datetime import datetime

import lrpy as lp


local_preddir = 'pred'


def pred(sched_str):
    """
    Generate tracking file for a given sched str.

    The filename is '{date}T{stime}.{ext}', such as '20181024T002157.aji'.

    Parameters
    ----------
    sched_str : str
        the sched str, in format '{datestr} {station_id} {target_ext}
        {timestr_open} {timestr_close} {maxele} {timestr_maxele}
        {cpf_filename}', such as '20181024 7819   aji 002157 003520 35
        002837 ajisai_cpf_181024_7971.jax'
    """
    # '20181024 7819   aji 002157 003520 35 002837 ajisai_cpf_181024_7971.jax'
    # out_filename = 20181024T002157.aji
    sched_str = sched_str.strip()
    print('\nGenerating tracking file for the sched:\n{}'.format(sched_str))
    [date, station_id, ext, stime, etime, maxele, mtime, filename] = \
        re.split(r'\s+', sched_str)

    start_datetime = datetime.strptime(date + stime, '%Y%m%d%H%M%S')
    end_datetime = datetime.strptime(date + etime, '%Y%m%d%H%M%S')
    out_filename = '{}T{}.{}'.format(date, stime, ext)

    # offline mode: use all valid cpf file
    abs_filename = '{}/{}'.format(cpf_dir, filename)
    # abs_filename = lp.util.cpf_absolute_filename(filename)

    print('The cpf file is: {}'.format(abs_filename))

    cpf = lp.cpf.CPF(abs_filename)

    # prepare absolute path of tracking file
    year = int(date[0:4])
    rel_dir = '{}/{}'.format(str(year), date)
    local_dir = '{}/{}'.format(local_preddir, rel_dir)
    if not os.path.exists(local_dir):
        os.makedirs(local_dir, exist_ok=True)

    abs_out_filename = '{}/{}'.format(local_dir, out_filename)

    print('The out file is: {}'.format(abs_out_filename))

    lp.predict_all(cpf, lp.default_station,
                   start_datetime, end_datetime,
                   interval='1s',
                   filename=abs_out_filename,
                   formatter=lp.core.default_trackdata_formatter)


if __name__ == '__main__':
    config = lp.load_config()
    local_preddir = config['paths']['local_preddir']
    local_cpfdir = '{}/{}'.format(lp.local_rootdir, lp.relative_cpfdir)

    date = datetime.utcnow()

    sch_filename = date.strftime('%Y/%Y%m%d/%Y%m%d.sch')
    sch_filename = '{}/{}'.format(local_preddir, sch_filename)

    cpf_dir = '{}/{}/{}'.format(
            local_cpfdir, 'offline', date.strftime('%Y%m%d'))

    passes = None

    if os.path.exists(sch_filename):
        # the arg is a filename
        with open(sch_filename) as f:
            passes = f.readlines()

    if passes:
        for sched_str in passes:
            pred(sched_str)
