#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
sched app.

@author: lirw (lirw@ynao.ac.cn)
"""

import os
import re
from datetime import datetime

import lrpy as lp

local_preddir = 'pred'


if __name__ == '__main__':
    config = lp.load_config()
    local_preddir = config['paths']['local_preddir']
    local_cpfdir = '{}/{}'.format(lp.local_rootdir, lp.relative_cpfdir)

    print('offline mode...')
    all_pass = []
    date = datetime.utcnow()
    # offline mode: use all valid cpf file
    cpf_dir = config['paths']['cpf_dir']
    if not cpf_dir:
        cpf_dir = '{}/{}/{}'.format(
                local_cpfdir, 'offline', date.strftime('%Y%m%d'))
    filenames = os.listdir(cpf_dir)
    if not filenames:
        print('!!ERROR: NO cpf files in dir {}'.format(cpf_dir))
    else:
        for filename in filenames:
            # print(filename)
            # check filename for valid cpf, 'satellite_cpf_yymmdd_nnnv.src'
            r = re.match(r'^([a-zA-Z0-9]+)_cpf_(\d+)_(\d+).(\w+)$', filename)
            if not r:
                continue

            cpf_filename = '{}/{}'.format(cpf_dir, filename)
            cpf = lp.cpf.CPF(cpf_filename)
            pass_list = lp.sched(cpf)

            for npass in pass_list:
                print(npass)
                all_pass.append(npass)

        if all_pass:
            # output sched
            year = date.year
            rel_dir = '{}/{}'.format(str(year), date.strftime('%Y%m%d'))
            local_dir = '{}/{}'.format(local_preddir, rel_dir)
            if not os.path.exists(local_dir):
                os.makedirs(local_dir, exist_ok=True)
            out_filename = '{}.sch'.format(date.strftime('%Y%m%d'))
            abs_out_filename = '{}/{}'.format(local_dir, out_filename)

            with open(abs_out_filename, 'w') as out:
                pass_list.sort(key=lambda npass: npass.open_trackdata.epoch)
                for npass in all_pass:
                    out.write(repr(npass))
                    out.write('\n')
                    print(npass)
        else:
            print('empty pass list')
