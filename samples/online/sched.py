#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
sched app.

@author: lirw (lirw@ynao.ac.cn)
"""

import os
import sys
import re
from datetime import datetime, timedelta

import lrpy as lp

local_preddir = 'pred'


def sched(target_names=None, date_str=None):
    if target_names is None:
        return

    if date_str is None:
        date = datetime.utcnow()
        start_datetime = None
        end_datetime = None
    else:
        date = datetime.strptime(date_str, '%y%m%d')
        start_datetime = date + timedelta(hours=lp.start_hours)
        end_datetime = date + timedelta(hours=lp.end_hours)

    all_pass = []

    for target_name in target_names:
        # check if target_name is valid
        satellite = lp.satellite_by_name(target_name)
        if satellite is None:
            print('ERROR: {} is not a valid satellite'.format(target_name))
        else:
            cpf_filenames = lp.util.list_cpf_filenames(
                    target_name, date_str=date_str)
            if len(cpf_filenames) > 0:
                cpf = lp.cpf.CPF(cpf_filenames[0])
                pass_list = lp.sched(
                        cpf,
                        start_datetime=start_datetime,
                        end_datetime=end_datetime)

                for npass in pass_list:
                    all_pass.append(npass)

    if all_pass:
        # output sched
        year = date.year
        rel_dir = '{}/{}'.format(str(year), date.strftime('%Y%m%d'))
        local_dir = '{}/{}'.format(local_preddir, rel_dir)
        if not os.path.exists(local_dir):
            os.makedirs(local_dir, exist_ok=True)
        out_filename = '{}.sch'.format(date.strftime('%Y%m%d'))
        abs_out_filename = '{}/{}'.format(local_dir, out_filename)

        with open(abs_out_filename, 'w') as out:
            pass_list.sort(key=lambda npass: npass.open_trackdata.epoch)
            for npass in all_pass:
                out.write(repr(npass))
                out.write('\n')
                print(npass)
    else:
        print('empty pass list')


if __name__ == '__main__':
    config = lp.load_config()
    local_preddir = config['paths']['local_preddir']
    local_cpfdir = '{}/{}'.format(lp.local_rootdir, lp.relative_cpfdir)

    satellites = config['pred']['satellites']
    if satellites:
        satellites = re.split(r',\s+', satellites)
    else:
        satellites = ['ajisai', 'lageos1', 'lageos2', 'beaconc']

    if len(sys.argv) >= 2:
        arg1 = sys.argv[1]
        if arg1 == '-h':
            print('Usage: python sched.py [date_str]\n')
            print('Finding passes for specified satellites.')
            print('date_str is in format of "%y%m%d", such as "181025".')
            print('The sch file (output) is "{}/%Y/%Y%m%d/%Y%m%d.sch",'.format(
                    local_preddir))
            print('such as "{}/2018/20181025/20181025.sch".'.format(
                    local_preddir))
            exit(0)

        date_str = arg1
    else:
        date_str = datetime.utcnow().strftime('%y%m%d')

    sched(satellites, date_str)
