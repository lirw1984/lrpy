#!/usr/bin/env python3
"""
Core lib for cpf algorithm (both slr and llr), including sched, pred, etc

Classes:
    TrackData
    A class to handle tracking data, including pointing(azimuth, elevation),
    flight time for laser raning, different in azimuth and elevation between
    transmitter and reciever, also sunlit or not.

    Pass
    A class to handle pass, which contanins open, close, highest, and the
    related station, target, cpf filename

Functions:
    clkcor          Return clock correction for the relativistic differential
                    relationship
    mjd_sod         Return mjd and sod for a given UTC datetime
    sched           Find pass schedule for the given cpf and station.
    predict_all     Generate tracking data for the given cpf and station from
                    start_datetime to end_datetime.
    predict         Compute a TrackData for slr at given datetime.
    predict_llr     Compute a TrackData for llr at given datetime.


    default_trackdata_formatter     Return a formatted string for trackdata
                                    to generate a tracking file

@author: lirw (lirw@ynao.ac.cn)
"""
import os
import sys
import math
import re
from datetime import datetime, timedelta
import logging

from .math3d import Vector3d, Matrix33
from .constants import MJD_J2000, SECS_PER_DAY, AU, SPEED_OF_LIGHT, \
                        WGS84_EARTH_ANGULAR_VELOCITY
import lrpy as lp

__twopi__ = 2 * math.pi


def clkcor(mjdutc, geolatitude, longitude):
    """
    Return clock correction for the relativistic differential relationship.

    It is used to convert an interval of proper or atomic time to
    an interval of coordinate time (see Mulholland, 1972 PASP, 84, P.357.)

    This effect is about 1.3 nsec for the moon  but hundreds of nsec for Mars.

    Parameters
    ----------
    datetime: datetime, UTC
        datetime to calculate mjd and sod

    geolatitude: float, rad
        geolatitude

    longitude: float, rad
        longitude

    Returns
    ----------
    clock correction in seconds
    """
    dd = mjdutc - 15019.5
    bd = dd * 1.0e-4
    bd2 = bd * bd
    bd3 = bd * bd2

    ems = (358.475845e0 + 0.985600267e0 * dd - 1.12e-05 * bd2 - 0.7e-07 * bd3)
    ems = math.radians(ems)
    ecs = 0.01675104e0 - 1.1444e-05 * bd - 9.4e-09 * bd2
    u = ems + ecs * math.sin(ems)
    ut = math.fmod(mjdutc, 1.0) * math.pi * 2
    cosu = math.cos(u)

    dtadt = 1.0e0 - 3.302e-10 * cosu / (1.0e0 - ecs * cosu) \
        - 1.476e-10 * math.cos(geolatitude) * math.cos(ut + longitude)

    return dtadt


def mjd_sod(datetime):
    """
    Return mjd and sod for a given UTC datetime.

    Parameters
    ----------
    datetime: datetime, UTC
        datetime to calculate mjd and sod

    Returns
    ----------
    (mjd, sod), mjd is integer, and sod is a float
    """
    # 678576=1858-11-17T0h
    mjd = int(datetime.toordinal() - 678576.0)
    sod = (datetime.hour * 3600 + datetime.minute * 60 + datetime.second
           + datetime.microsecond * 1e-6)

    return (mjd, sod)


def default_trackdata_formatter(trackdata, pre_trackdata=None):
    """
    Return a formatted string for trackdata to generate a tracking file.

    Parameters
    ----------
    trackdata: TrackData
        a trackdata used to construct the formatted string.

    pre_trackdata: TrackData
        previous trackdata to calculate rate in azimuth and elevation.
        If None, the both rates are fixed to zero.

    Returns
    ----------
    a formatted string for trackdata
    """
    if pre_trackdata is not None:
        dt = (trackdata.epoch - pre_trackdata.epoch).total_seconds()
        azimuth_rate = trackdata.azimuth_out - pre_trackdata.azimuth_out
        elevation_rate = trackdata.elevation_out - pre_trackdata.elevation_out

        if azimuth_rate > math.pi:
            azimuth_rate -= __twopi__
        elif azimuth_rate < -math.pi:
            azimuth_rate += __twopi__

        azimuth_rate /= dt
        elevation_rate /= dt
    else:
        azimuth_rate = 0.0
        elevation_rate = 0.0

    # format
    (mjd, sod) = mjd_sod(trackdata.epoch)
    outstr = \
        '{}.{:03d} {:8.4f} {:8.4f} {:8.4f} {:8.4f}  6.0   {:12.8f} {:14.10f}' \
        .format(trackdata.epoch.strftime('%H %M %S'),
                int(trackdata.epoch.microsecond/1000),
                math.degrees(trackdata.azimuth_out),
                math.degrees(trackdata.elevation_out),
                math.degrees(azimuth_rate),
                math.degrees(elevation_rate),
                sod / SECS_PER_DAY, trackdata.flight_time)
    return outstr


class TrackData:
    """
    A class to handle tracking data, including pointing(azimuth, elevation),
    flight time for laser raning, different in azimuth and elevation between
    transmitter and reciever, also sunlit or not.
    """
    def __init__(self, epoch, azimuth_out, elevation_out,
                 dif_azimuth, dif_elevation, flight_time, sunlit):
        self._epoch = epoch
        self._azimuth_out = azimuth_out
        self._elevation_out = elevation_out
        self._dif_azimuth = dif_azimuth
        self._dif_elevation = dif_elevation
        self._flight_time = flight_time
        self._sunlit = sunlit

        # (z, mjd) = gcal2jd(datetime.year, datetime.month, datetime.day)
        (mjd, sod) = mjd_sod(epoch)
        self._mjd = mjd + sod / 86400.0

        pass

    @property
    def mjd(self):
        """Return the mjd of transmitting time, float"""
        return self._mjd

    @property
    def epoch(self):
        """Return the datetime of transmitting, datetime"""
        return self._epoch

    @property
    def azimuth_out(self):
        """Return the azimuth of transmitting(out), in rad"""
        return self._azimuth_out

    @property
    def elevation_out(self):
        """Return the elevation of transmitting(out), in rad"""
        return self._elevation_out

    @property
    def dif_azimuth(self):
        """Return the different in azimuth between transmitter
        and reciever, in rad
        """
        return self._dif_azimuth

    @property
    def dif_elevation(self):
        """Return the different in elevation between transmitter
        and reciever, in rad
        """
        return self._dif_elevation

    @property
    def flight_time(self):
        """Return the flight time for laser ranging, in seconds"""
        return self._flight_time

    @property
    def azimuth_in(self):
        """Return the azimuth of receiver(in), in rad"""
        return math.fmod(self._azimuth_out + self._dif_azimuth, math.pi * 2)

    @property
    def elevation_in(self):
        """Return the elevation of receiver(in), in rad"""
        return self._elevation_out + self._dif_elevation

    def __repr__(self):
        return '{:.5f} {:11.5f} {:11.5f} {:9.5f} {:9.5f} {:14.10f} {}'.format(
                self._mjd, math.degrees(self._azimuth_out),
                math.degrees(self._elevation_out),
                math.degrees(self._dif_azimuth),
                math.degrees(self._dif_elevation),
                self._flight_time, self._epoch)


class Pass:
    """
    A class to handle pass, which contanins open, close, highest, and the
    related station, target, cpf filename
    """
    def __init__(self, station, target_ext, cpf_filename,
                 open_trackdata, close_trackdata, highest_trackdata):
        self._station = station
        self._target_ext = target_ext
        self._cpf_filename = cpf_filename

        self._open_trackdata = open_trackdata
        self._close_trackdata = close_trackdata
        self._highest_trackdata = highest_trackdata

    def __repr__(self):
        open_datetime = self._open_trackdata.epoch
        datestr = '{:04d}{:02d}{:02d}'.format(
                open_datetime.year, open_datetime.month, open_datetime.day)

        return '{} {:4d} {:>6} {} {} {:.0f} {} {}'.format(
                datestr, self._station.id, self._target_ext,
                self._open_trackdata.epoch.strftime('%H%M%S'),
                self._close_trackdata.epoch.strftime('%H%M%S'),
                math.degrees(self._highest_trackdata.elevation_out),
                self._highest_trackdata.epoch.strftime('%H%M%S'),
                self._cpf_filename
            )

    # Read-only field accessors
    @property
    def open_trackdata(self):
        """Return the Trackdata related to open point of the pass"""
        return self._open_trackdata

    @property
    def close_trackdata(self):
        """Return the Trackdata related to close point of the pass"""
        return self._close_trackdata

    @property
    def highest_trackdata(self):
        """Return the Trackdata related to highest point of the pass"""
        return self._highest_trackdata


def sun_position(datetime):
    """
    Return the sun position in J2000? at the given datetime (UTC)
    """
    (mjd, sod) = mjd_sod(datetime)
    dt = mjd - MJD_J2000 + sod / SECS_PER_DAY

    # mean longitude of SUN (corrected for aberration)
    xsml = math.radians((280.460 + 0.9856474 * dt) % 360.0)

    # mean anomaly
    xsma = math.radians((357.528 + 0.9856003*dt) % 360.0)
    sinma = math.sin(xsma)
    cosma = math.cos(xsma)
    sinma2 = math.sin(2 * xsma)
    cosma2 = math.cos(2 * xsma)

    # ecliptic longitude
    xsel = xsml + math.radians(1.915 * sinma + 0.020 * sinma2)

    # obliquity of ecliptic (ecliptic latitude=0.D0)
    xsoe = math.radians(23.439 - 0.0000004 * dt)

    # distance of SUN from EARTH
    rsun = AU * (1.00014 - 0.01671 * cosma - 0.00014 * cosma2)

    # rectangular coordinates of SUN
    x = rsun * math.cos(xsel)
    y = rsun * math.cos(xsoe) * math.sin(xsel)
    z = rsun * math.sin(xsoe) * math.sin(xsel)

    return Vector3d(x, y, z)


def predict_all(cpf, station, start_datetime, end_datetime,
                interval='1m', mode=1, station_rcv=None,
                filename=None, min_elevation=0, formatter=None):
    """
    Generate tracking data for the given cpf and station from
    start_datetime to end_datetime.

    Parameters
    ----------
    cpf: CPF
        a CPF instance.

    station: Station
        a station to generate tracking data.

    start_datetime: datetime
        datetime to start tracking data generating.

    end_datetime: datetime
        datetime to end tracking data generating.

    interval: int or str
        interval of trackdata. If it is int, then the unit is miliseconds.
        Or it is str, then a str with a int value and a unit character,
        'h' for hours, 'm' for minutes, 's' for seconds, 'ms' for miliseconds,
        such as '1s', '15m', '20ms', etc.

    mode: int, [0,1,2]
        0 for instant vector only, 1 for outbound, 2 for inbound.

    station_rcv: Station
        a station as reciever to compute the trackdata, for mode=2 only.
        If None, station_rcv=station

    filename: str
        the filename to output the tracking data.
        If None, output to the sys.stdout.

    min_elevation: float, degrees
        minimum elevation to output.

    formatter: function
        a function to format trackdata, the parameters should be
        (trackdata, pre_trackdata). See also default_trackdata_formatter.
        If None, use repr() of TrackData.

    Returns
    ----------
    a TrackData list
    """
    if start_datetime < cpf.start_datetime:
        start_datetime = cpf.start_datetime

    if end_datetime > cpf.end_datetime:
        end_datetime = cpf.end_datetime

    # deg --> radians
    min_elevation = math.radians(min_elevation)

    # handle interval
    if type(interval) is (int):
        stepT = timedelta(milliseconds=interval)
    else:
        result = re.findall(r'\d+|\w+', interval)
        length = len(result)
        if length >= 1:
            val = int(result[0])
            if length >= 2:
                unit = result[1]
            else:
                unit = 'ms'
            if unit == 'h':
                stepT = timedelta(hours=val)
            elif unit == 'm':
                stepT = timedelta(minutes=val)
            elif unit == 's':
                stepT = timedelta(seconds=val)
            elif unit == 'ms':
                stepT = timedelta(milliseconds=val)
            else:
                stepT = timedelta(milliseconds=val)

    logging.info('generate tracking file within ({}~{}, {}) for {}({})'.format(
            start_datetime.strftime('%Y-%m-%dT%H:%M:%S'),
            end_datetime.strftime('%Y-%m-%dT%H:%M:%S'),
            interval,
            cpf.target_name, os.path.basename(cpf.filename)))

    trackdatas = []

    dt = start_datetime

    while dt <= end_datetime:
        if cpf.target_type == 1:
            # passive (retro-reflector) artificial satellite
            trackdata = predict(
                cpf, station, dt, mode=1, station_rcv=station_rcv)
        elif cpf.target_type == 2:
            # passive (retro-reflector) lunar reflector
            trackdata = predict_llr(cpf, station, dt, mode=mode)

        trackdatas.append(trackdata)

        dt = dt + stepT

    if filename is not None:
        out = open(filename, 'w')
        logging.info('output to the file: {}'.format(filename))
    else:
        out = sys.stdout

    pre_trackdata = None
    for trackdata in trackdatas:
        if trackdata.elevation_out > min_elevation:
            if formatter is None:
                out.write(repr(trackdata))
            else:
                out.write(formatter(trackdata, pre_trackdata))
            out.write('\n')
        pre_trackdata = trackdata

    if out is not sys.stdout:
        # if fout is not a file
        out.close()

    return trackdatas


def predict(cpf, station_trt, datetime, mode=1, station_rcv=None):
    """
    Compute a TrackData for slr at given datetime.

    Parameters
    ----------
    cpf: CPF
        a CPF instance (slr).

    station_trt: Station
        a station as transmitter to compute the trackdata.

    datetime: datetime
        datetime to compute the trackdata.

    mode: int, [0,1,2]
        0 for instant vector only, 1 for outbound, 2 for inbound.

    station_rcv: Station
        a station as reciever to compute the trackdata, for mode=2 only.
        If None, station_rcv=station

    Returns
    ----------
    a TrackData for llr at given datetime.
    """
    # datetime --> (mjd, sod)
    (mjd, sod) = mjd_sod(datetime)
    t = (mjd, sod)

    # interpolation at t (mjd, sod) using cpf, itrf
    (px, py, pz) = cpf.interpolate_position(t)
    itrf_trt = Vector3d(px, py, pz)

    # transform to topcentric frame for transmit station
    top_trt = station_trt.trans(itrf_trt)

    dist_trt = top_trt.length
    elevation_trt = math.asin(top_trt.z / dist_trt)
    azimuth_trt = math.atan2(-top_trt.y, top_trt.x)
    if azimuth_trt < 0:
        azimuth_trt += math.pi * 2

    flight_time = 2 * dist_trt / SPEED_OF_LIGHT

    sunlit = True

    if mode == 0:
        # instant vector only
        return TrackData(datetime, azimuth_trt, elevation_trt,
                         0.0, 0.0, flight_time, sunlit)
    else:
        # stageo_trt: ITRF at transmit time (t)
        stageo_trt = station_trt.R
        stageo_trt_rot = stageo_trt

        tout = dist_trt / SPEED_OF_LIGHT
        # iteration: bounce time
        for k in range(0, 2):
            tb = (mjd, sod + tout)  # t --> tb = t + tout

            # bnc: ITRF at bounce time (tb)
            (px_bnc, py_bnc, pz_bnc) = cpf.interpolate_position(tb)
            itrf_bnc = Vector3d(px_bnc, py_bnc, pz_bnc)

            # same frame: ITRF at bounce time (tb)
            top_out = itrf_bnc - stageo_trt_rot
            dist_out = top_out.length
            tout = dist_out / SPEED_OF_LIGHT

            # rotate station during flight time
            # stageo_rot: ITRF at t --> ITRF at tb
            dsidt = WGS84_EARTH_ANGULAR_VELOCITY * tout
            m = Matrix33.rotation(3, dsidt)  # due to rotation of earth
            stageo_trt_rot = m * stageo_trt

        # rotation to topocentric without trans origin
        # topocentric at transmit time (t)
        top_bnc = station_trt.trans(top_out, True)
        dist_out = top_bnc.length
        elevation_out = math.asin(top_bnc.z / dist_out)
        azimuth_out = math.atan2(-top_bnc.y, top_bnc.x)
        if azimuth_out < 0:
            azimuth_out += math.pi * 2

        # total point ahead / behind
        # difference between receive and transmit direction at t
        dif_azimuth = 2 * (azimuth_trt - azimuth_out)
        dif_elevation = 2 * (elevation_trt - elevation_out)

        if mode == 1:
            # outbound mode
            # average distance from station to object (both at tb)
            obj = itrf_bnc - stageo_trt
            dist_out = obj.length
            flight_time = 2 * dist_out / SPEED_OF_LIGHT

            return TrackData(datetime, azimuth_out, elevation_out,
                             dif_azimuth, dif_elevation, flight_time, sunlit)
        else:  # mode=2
            # inbound vector: station at receiving time
            tinb = tout
            if station_rcv is not None:
                # using different station to trt and rcv
                stageo_rcv = station_rcv.R
            else:
                # using single station to trt and rcv
                stageo_rcv = stageo_trt

            for k in range(0, 2):
                # t --> tr = tb + tinb = t + tout + tinb
                tr = (mjd, sod + tout + tinb)

                # rotate station during inbound flight time
                # stageo_rot: ITRF at tr --> ITRF at tb
                dsidt = WGS84_EARTH_ANGULAR_VELOCITY * tinb
                m = Matrix33.rotation(3, -dsidt)  # due to rotation of earth
                stageo_rcv_rot = m * stageo_rcv

                # same frame: ITRF at bounce time (tb)
                top_inb = itrf_bnc - stageo_rcv_rot
                dist_inb = top_inb.size
                tinb = dist_inb / SPEED_OF_LIGHT

            dist_tot = dist_out + dist_inb
            flight_time = dist_tot / SPEED_OF_LIGHT

            return TrackData(datetime, azimuth_out, elevation_out,
                             dif_azimuth, dif_elevation, flight_time, sunlit)


def sched(cpf, station=None, start_datetime=None, end_datetime=None,
          openclose_elevation=None, valid_pass_highest_elevation=None):
    """
    Find pass schedule for the given cpf and station.

    Parameters
    ----------
    cpf: CPF
        a CPF instance.

    station: Station
        a station to find the pass.

    start_datetime: datetime
        datetime to start pass finding.
        if None, 0h of today plus lp.start_hours is used.

    end_datetime: datetime
        datetime to end pass finding.
        if None, 0h of today plus lp.end_hours is used.

    openclose_elevation: float, degrees
        elevation to define the pass open and close.
        if None, lp.openclose_elevation is used.

    valid_pass_highest_elevation: float, degrees
        the minimum of highest elevation to define a valid pass,
        0 for no limitation. if None, lp.valid_pass_highest_elevation is used.

    Returns
    ----------
    a pass list
    """
    if station is None:
        station = lp.default_station

    today = datetime.utcnow()
    today = datetime(today.year, today.month, today.day)
    if start_datetime is None:
        start_datetime = today + timedelta(hours=lp.start_hours)
    if end_datetime is None:
        end_datetime = today + timedelta(hours=lp.end_hours)

    if openclose_elevation is None:
        openclose_elevation = lp.openclose_elevation

    if valid_pass_highest_elevation is None:
        valid_pass_highest_elevation = lp.valid_pass_highest_elevation

    if start_datetime < cpf.start_datetime:
        start_datetime = cpf.start_datetime

    if end_datetime > cpf.end_datetime:
        end_datetime = cpf.end_datetime

    logging.info('do sched with ({}~{}) for {}({})'.format(
            start_datetime.strftime('%Y-%m-%dT%H:%M:%S'),
            end_datetime.strftime('%Y-%m-%dT%H:%M:%S'),
            cpf.target_name, os.path.basename(cpf.filename)))

    # deg --> radians
    openclose_elevation = math.radians(openclose_elevation)
    valid_pass_highest_elevation = math.radians(valid_pass_highest_elevation)

    pass_list = []
    target_ext = cpf.target_ext
    cpf_filename = os.path.basename(cpf.filename)

    dt = start_datetime
    stepT_small = timedelta(seconds=1)
    stepT_big = timedelta(seconds=30)

    stepT = stepT_big
    bsmall = False

    brise = False
    bset = False
    breach = False
    bdrop = False
    rise_trackdata = None
    set_trackdata = None
    open_trackdata = None
    close_trackdata = None
    enter_trackdata = None
    exit_trackdata = None
    highest_trackdata = None

    last_trackdata = None

    while dt <= end_datetime:
        if cpf.target_type == 1:
            # passive (retro-reflector) artificial satellite
            trackdata = predict(cpf, station, dt, mode=0)
        elif cpf.target_type == 2:
            # passive (retro-reflector) lunar reflector
            trackdata = predict_llr(cpf, station, dt, mode=0)

        elevation_out = trackdata.elevation_out

        # print('%.4f %s %s' % (math.degrees(elevation_out), stepT, brise))

        if elevation_out > 0:  # above horizon
            if not brise:
                # first above horizon, that is coarse rise
                if not bsmall:  # big step --> small step
                    bsmall = True
                    dt = dt - stepT  # Note: back 1-step
                else:
                    # second above horizon, that is real rise.
                    # reset step --> big step
                    bsmall = False
                    brise = True
                    rise_trackdata = trackdata
                    # print('rise: %s' % (rise_trackdata))
            # NOTE: rise and open
            if elevation_out > openclose_elevation:
                # above openclose_elevation
                if not breach:  # first above openclose_elevation
                    if not bsmall:  # big step --> small step
                        bsmall = True
                        dt = dt - stepT  # Note: back 1-step
                    else:
                        # second above openclose_elevation, that is real open.
                        # reset step --> big step
                        bsmall = False
                        breach = True
                        open_trackdata = trackdata
                        # print('open: %s' % (open_trackdata))

                if (highest_trackdata is None) or \
                   (elevation_out - highest_trackdata.elevation_out > 3.0e-10):
                    # 1.0e-6 deg
                    highest_trackdata = trackdata
            else:  # below openclose_elevation
                if breach and (not bdrop):
                    if not bsmall:  # big step --> small step
                        bsmall = True
                        dt = dt - stepT  # Note: back 1-step
                    else:
                        # second above openclose_elevation, that is real open.
                        # reset step --> big step
                        bsmall = False
                        bdrop = True
                        close_trackdata = last_trackdata
                        # print('%s' % (close_trackdata))
        else:  # below horizon
            if brise and (not bset):
                if not bsmall:  # big step --> small step
                    bsmall = True
                    dt = dt - stepT  # Note: back 1-step
                else:
                    # second above openclose_elevation, that is real open.
                    # reset step --> big step
                    bsmall = False
                    bset = True
                    set_trackdata = trackdata
                    # print('%s' % (set_trackdata))

                    # new pass
                    if (open_trackdata is not None) \
                            and (close_trackdata is not None):
                        # find max_ele more exactly
                        # print('##: %s' % (highest_trackdata))
                        dt_me = highest_trackdata.epoch - stepT_big
                        dt_me_end = highest_trackdata.epoch + stepT_big

                        while dt_me <= dt_me_end:
                            if cpf.target_type == 1:
                                # passive(retro-reflector) artificial satellite
                                trackdata = predict(cpf, station, dt_me,
                                                    mode=0)
                            elif cpf.target_type == 2:
                                # passive (retro-reflector) lunar reflector
                                trackdata = predict_llr(cpf, station, dt_me,
                                                        mode=0)

                            delt_ele = trackdata.elevation_out \
                                - highest_trackdata.elevation_out
                            if (delt_ele > 3.0e-10):
                                # 1.0e-6 deg
                                highest_trackdata = trackdata
                            dt_me += stepT_small

                        # except not exceed openclose_elevation
                        new_pass = Pass(station, target_ext, cpf_filename,
                                        open_trackdata, close_trackdata,
                                        highest_trackdata)
                        pass_list.append(new_pass)

                    # reset flags
                    bsmall = False
                    stepT = stepT_big
                    brise = False
                    bset = False
                    breach = False
                    bdrop = False
                    rise_trackdata = None
                    set_trackdata = None
                    open_trackdata = None
                    close_trackdata = None
                    enter_trackdata = None
                    exit_trackdata = None
                    highest_trackdata = None

        last_trackdata = trackdata

        if bsmall:
            stepT = stepT_small
        else:
            stepT = stepT_big
        # dt add stepT
        dt += stepT

        if dt < start_datetime:
            dt = start_datetime

    # excepts
    if brise and (not bset):
        bset = True
        set_trackdata = last_trackdata

        if close_trackdata is None:
            close_trackdata = set_trackdata

        if highest_trackdata is not None:
            #  new pass
            new_pass = Pass(station, target_ext, cpf_filename,
                            open_trackdata, close_trackdata,
                            highest_trackdata)

            pass_list.append(new_pass)

    return pass_list


def predict_llr(cpf, station, datetime, mode=1):
    """
    Compute a TrackData for llr at given datetime.

    Parameters
    ----------
    cpf: CPF
        a CPF instance (llr).

    station: Station
        a station to compute the trackdata.

    datetime: datetime
        datetime to compute the trackdata.

    mode: int, [0,1,2]
        0 for instant vector only, 1 for outbound, 2 for inbound.

    Returns
    ----------
    a TrackData for llr at given datetime.
    """
    # datetime --> (mjd, sod)
    (mjd, sod) = mjd_sod(datetime)
    # relativistic: atomic time ==> an interval of coordinate time
    dtadt = clkcor(mjd + sod / 86400.0, station.geolatitude, station.longitude)

    # outbound
    # iter
    dt = 0.0
    while True:
        olddt = dt
        tt = (mjd, sod - dt)

        # interpolation pos at tt (mjd, sod-dt) using cpf, itrf
        (px, py, pz) = cpf.interpolate_position(tt)
        itrf_out = Vector3d(px, py, pz)

        # transform to topcentric frame
        top_out = itrf_out - station.R
        rnger = itrf_out.length
        dist12 = top_out.length

        dt = (rnger - dist12) / SPEED_OF_LIGHT

        if math.fabs(dt - olddt) <= 1.0e-10:
            break

    # Point: topocentric az/el
    # plus aberr
    (aberx, abery, aberz, corr12) = cpf.interpolate_corrections(tt)
    bodtel = top_out + Vector3d(aberx, abery, aberz)

    # trans to topocentric frame
    top_out = station.trans(bodtel, True)  # rotation only
    elevation_out = math.asin(top_out.z / top_out.length)
    azimuth_out = math.atan2(-top_out.y, top_out.x)
    if azimuth_out < 0:
        azimuth_out += math.pi * 2

    # Inbound
    tinb = (mjd, sod - olddt)
    (px_back, py_back, pz_back) = cpf.interpolate_position(
        tinb, backleg=True)
    itrf_in = Vector3d(px_back, py_back, pz_back)
    top_in = itrf_in + station.R
    dist23 = top_in.length
    # NOTE: minus aberr
    (aberx_back, abery_back, aberz_back, corr23) = \
        cpf.interpolate_corrections(tinb, backleg=True)
    bodtel = -1.0 * (top_in - Vector3d(aberx_back, abery_back, aberz_back))

    # trans to topocentric frame
    top_in = station.trans(bodtel, True)  # rotation only
    elevation_in = math.asin(top_in.z / top_in.length)
    azimuth_in = math.atan2(-top_in.y, top_in.x)
    if azimuth_in < 0:
        azimuth_in += math.pi * 2

    # flight time, corr12 and corr23 are in nsec
    flight_time = (dist12 + dist23) / SPEED_OF_LIGHT + \
        (corr12 - corr23) * 1.0e-9 + 0.27e-9
    flight_time = flight_time * dtadt

    dif_azimuth = azimuth_in - azimuth_out
    dif_elevation = elevation_in - elevation_out
    sunlit = True

    return TrackData(datetime, azimuth_out, elevation_out,
                     dif_azimuth, dif_elevation, flight_time, sunlit)
