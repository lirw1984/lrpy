#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Consolidated Laser Ranging Data Format (CRD) v1.01

@author: lirw (lirw@ynao.ac.cn)
"""

import os
import re
from ctypes import c_double
from datetime import datetime

import lrpy as lp
from lrpy.lagrange import lagrange
from lrpy.satellite import satellite_names, satellite_by_id


class CRD:
    """
    A class for handling CPF file.
    """


class H1:
    """
    Format Header。

    The format header describes information relating to the file.
    """
    def __init__(self, format_version, production_datetime):
        if format_version is None:
            self._format_version = format_version
        else:  # default is 1
            self._format_version = 1

        self._production_datetime = production_datetime

    @classmethod
    def strph1(cls, h1_string):
        """string --> new H1 parsed from a string."""
        data = h1_string.split(None)
        errors = []

        if data[0].upper() != 'H1':
            errors.append('Exxxxx')

        if data[1].upper() != 'CRD':
            errors.append('EH1021')

        format_version = int(data[2])

        try:
            year = int(data[3])
        except ValueError:
            year = -1

        if not (1950 <= year <= 2100):
            errors.append('EH1051')

        try:
            month = int(data[4])
            day = int(data[5])
            hour = int(data[6])
            production_datetime = datetime(year, month, day, hour)
        except ValueError:
            errors.insert(-1, 'EH1002')
            if not (1 <= month <= 12):
                errors.append('EH1061')

            if not (1 <= day <= 31):
                errors.append('EH1071')

            if not (1 <= hour <= 23):
                errors.append('EH1081')

        if errors:
            raise ValueError(errors)

        return cls(format_version, production_datetime)

    def __repr__(self):
        return '{} {} {:>2} {:>13}'.format(
            'H1', 'CRD', self.format_version,
            self._production_datetime.strftime('%Y %m %d %H'),
            )

    def __str__(self):
        return '{} {}'.format(
            self.format_version,
            self._production_datetime.strftime('%Y-%m-%dT%H'))

    # Read-only field accessors
    @property
    def format_version(self):
        """Return format version, int. It is mostly 1."""
        return self._format_version

    @property
    def production_datetime(self):
        """Return production_datetime, datetime."""
        return self._production_datetime


class H2:
    """
    Station Header.

    The station header describes information relating to the station or
    site collecting this laser data.
    """
    def __init__(self, station_name, station_id, time_scale):
        self._station_name = station_name
        self._station_id = station_id
        self._time_scale = time_scale

    @classmethod
    def strph1(cls, h1_string):
        """string --> new H2 parsed from a string."""
        data = h1_string.split(None)
        errors = []

        if data[0].upper() != 'H2':
            errors.append('Exxxxx')

        station_name = data[1]
        station_id = int(data[2])
        cdp_num = int(data[3])
        cpd_seq = int(data[4])

        time_scale = int(data[5])

        if errors:
            raise ValueError(errors)

        return cls(station_name, station_id, time_scale)

    def __repr__(self):
        return '{} {:<10} {:>2} {:>13}'.format(
            'H2', self.station_name, self.station_id,
            self.time_scale
            )

    def __str__(self):
        return '{} {}'.format(
            self.station_name,
            self.station_id, self.time_scale)

    # Read-only field accessors
    @property
    def station_name(self):
        """Return station name."""
        return self._station_name

    @property
    def station_id(self):
        """Return station id."""
        return self._station_id

    @property
    def time_scale(self):
        """Return time scale, ints."""
        return self._time_scale
