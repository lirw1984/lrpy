#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
ILRS satellites manager.

Satellite in json format (dict like), the sample is as followings:
    {"status": "present", "np_indicator": "5",
     "satellite_name": "ajisai", "bin_size": "30",
     "satellite_id": "8606101", "ext": "aji",
     "sic_code": "1500", "norad_number": "16908"}

'satellite_names' is the set of official name of satellites.

When the module is inited, the json file 'data/satellites_all.json' is loaded.

Functions:
    satellite_by_id     Return a satellite by given id, None if not valid
    satellite_by_name   Return a satellite by given official name,
                        None if not valid
    satellite_by_norad  Return a satellite by given norad number,
                        None if not valid
    satellite_by_ext    Return a satellite by given official extention,
                        None if not valid

Examples:
    >>>import lrpy as lp
    >>>lp.satellite.satellite_by_id('7603901')
    {'status': 'present',
 'np_indicator': '7',
 'satellite_name': 'lageos1',
 'bin_size': '120',
 'satellite_id': '7603901',
 'ext': 'la1',
 'sic_code': '1155',
 'norad_number': '8820'}
    >>>lp.satellite.satellite_by_name('Geo-IK-2')
    {'status': 'present',
 'np_indicator': '5',
 'satellite_name': 'geoik2',
 'bin_size': '30',
 'satellite_id': '1603401',
 'ext': 'gi2',
 'sic_code': '5561',
 'norad_number': '41579'}
    >>>lp.satellite.satellite_by_norad(16908)
    {'status': 'present',
 'np_indicator': '5',
 'satellite_name': 'ajisai',
 'bin_size': '30',
 'satellite_id': '8606101',
 'ext': 'aji',
 'sic_code': '1500',
 'norad_number': '16908'}
    >>>lp.satellite.satellite_by_ext('bec')
    {'status': 'present',
 'np_indicator': '3',
 'satellite_name': 'beaconc',
 'bin_size': '15',
 'satellite_id': '6503201',
 'ext': 'bec',
 'sic_code': '0317',
 'norad_number': '1328'}

@author: lirw (lirw@ynao.ac.cn)
"""

import json
import os

__all__ = ['satellite_names', 'satellite_by_id', 'satellite_by_name',
           'satellite_by_norad', 'satellite_by_ext']

satellite_names = set()

try:
    """
    load json data, the format is as followings:

    {"status": "present", "np_indicator": "5",
     "satellite_name": "ajisai", "bin_size": "30",
     "satellite_id": "8606101", "ext": "aji",
     "sic_code": "1500", "norad_number": "16908"}
    """
    filename = os.path.dirname(__file__) + '/data/satellites_all.json'
    satellites = json.load(open(filename))
    for satellite in satellites:
        satellite_names.add(satellite['satellite_name'])
except FileNotFoundError:
    print('The file containing satellites info does NOT exist!', filename)
    satellites = []
    satellite_names = None
    raise ImportError("satellites info load faild. {}".format(filename))
except json.decoder.JSONDecodeError:
    print('The file containing satellites info is INVALID format!', filename)
    satellites = []
    satellite_names = None
    raise ImportError("satellites info load faild. {}".format(filename))


def satellite_by_id(satellite_id):
    """
    Return a satellite by given id, None if not valid.

    Parameters
    ----------
    satellite_id : str or int
        ILRS id of satellite, such as 103 for apollo15,
        '7603901' for lageos1.

    Returns
    ----------
    a satellite in json format
    """
    if isinstance(satellite_id, int):
        """e.g. 103 for apollo15"""
        satellite_id = '{:0>7}'.format(satellite_id)

    if len(satellite_id) < 7:
        """e.g. '803201' for jason2(0803201)"""
        satellite_id = '{:0>7}'.format(satellite_id)

    # search in the list
    for satellite in satellites:
        if satellite_id == satellite['satellite_id']:
            return satellite

    return None


def satellite_by_name(satellite_name):
    """
    Return a satellite by given official name, None if not valid.

    Parameters
    ----------
    satellite_name : str
        official name of satellite, such as 'ajisai', 'lageos1'.

    Returns
    ----------
    a satellite in json format
    """
    satellite_name = satellite_name.lower()
    satellite_name = satellite_name.replace('-', '')

    for satellite in satellites:
        if satellite_name == satellite['satellite_name']:
            return satellite

    return None


def satellite_by_norad(norad_number):
    """
    Return a satellite by given norad number, None if not valid.

    Parameters
    ----------
    norad_number : str or int
        norad number of satellite, such as 16908 for ajisai.

    Returns
    ----------
    a satellite in json format
    """
    if isinstance(norad_number, int):
        """e.g. 16908 for ajisai"""
        norad_number = str(norad_number)

    for satellite in satellites:
        if norad_number == satellite['norad_number']:
            return satellite

    return None


def satellite_by_ext(satellite_ext):
    """
    Return a satellite by given official extention, None if not valid.

    Parameters
    ----------
    satellite_ext : str or int
        official ext(short name) of satellite, such as 'aji' for ajisai,
        'bec' for beaconc, 'la1' for lageos1, 'ci3' for compassi3,
        '103g' for glonass103, 'g103' for galileo103.

    Returns
    ----------
    a satellite in json format
    """
    satellite_ext = satellite_ext.lower()

    for satellite in satellites:
        if satellite_ext == satellite['ext']:
            return satellite

    return None
