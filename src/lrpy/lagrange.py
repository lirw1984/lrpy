#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
lagrange interpolation

For performance, it is implemented by C to calculate the value of
lagrange basic functions.

The declaration is as followings:

    int lagrange_funcs(double xp, int nval, int count, double* x,
                              double* funcs, int* start_index)

    Input Parameters:
        xp              interpolated value
        nval            Points to be used
        count           total points in x array
        x               A 1-D array of real values

    Output Parameters:
        funcs           the value of lagrange basic functions at xp
        start_index     start index of used point in x array

    Return:
        0 for successful.
        -1 if xp is not within x array value range.

    Example:
        For the given x and xp:
        x = [86700.0, 87000.0, 87300.0, 87600.0, 87900.0, 88200.0,
             88500.0, 88800.0, 89100.0, 89400.0]
        xp = 88100.0

        the returned funcs and start_index should be:
        funcs = [0.000448, -0.005137, 0.028253, -0.105479, 0.395547, 0.791094,
                 -0.131849, 0.032290, -0.005651, 0.000483]
        start_index = 0


@author: lirw (lirw@ynao.ac.cn)
"""

import sys
import os
from ctypes import cdll, c_double, c_int, byref

# for performance, using ctypes to improve it.
if sys.platform.startswith('linux'):
    # Linux
    liblagrange = cdll.LoadLibrary(os.path.dirname(__file__) +
                                   '/lagrange.x86_64-linux-gnu.so')
elif sys.platform.startswith('win32'):
    # Windows
    liblagrange = cdll.LoadLibrary(os.path.dirname(__file__) +
                                   '/lagrange.win_amd64-msvc10.dll')
    print(liblagrange)
elif sys.platform.startswith('cygwin'):
    # Windows/Cygwin
    pass


def lagrange(xp, nval, c_x, y):
    """
    Return interpolated value of y at xp by lagrange formula.

    Parameters
    ----------
    xp : double
        interpolated value.

    nval : int
        Points to be used.

    c_x : (N,) array_like of c_double
        A 1-D array of real values.

    y : (...,N)
        A N-D array of real values. The length of `y` along the interpolation
        axis must be equal to the length of `x`.

    Returns
    -------
        Interpolated value.
    """
    # prepare var
    count = len(c_x)

    if type(c_x) is list:
        # if c_x is not a array_like of c_double but a list, convert it.
        x = c_x
        c_array_x = c_double * count
        c_x = c_array_x()
        for i in range(0, count):
            c_x[i] = x[i]

    c_array_type = c_double * nval
    c_funcs = c_array_type()
    c_start_index = c_int(0)

    # calculate lagrange basic functions
    if liblagrange is not None:
        ret = liblagrange.lagrange_funcs(
                c_double(xp), nval, count, byref(c_x),
                byref(c_funcs), byref(c_start_index))
    else:
        ret = lagrange_funcs(xp, nval, count, c_x, c_funcs, c_start_index)

    if ret == 0:  # success
        ydim = len(y)
        yp = [0] * ydim
        start_index = c_start_index.value

        # calcalute y by lagrange basic functions
        for i in range(0, nval):
            for k in range(0, ydim):
                yp[k] += y[k][i + start_index] * c_funcs[i]

        return yp
    else:  # error occured
        pass


def lagrange_funcs(xp, nval, count, x, c_funcs, c_start_index):
    return 0
