#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Station related, using WGS-84 system.

Classes:
    Station
    Ground station


Examples:
    >>>import lrpy as lp
    >>>lp.Station.from_position(
            lp.Vector3d(4331283.557, 567549.902, 4633140.353))
    7.465222 46.877230 951.331 4331283.557 567549.902 4633140.353 46.685206
    >>>s.R
    Vector3d(4331283.557000001, 567549.9020000001, 4633140.352999999)
    >>>s.mlt
    Matrix33(-0.7237040565868911, -0.09483058797410708, 0.6835638946464061;
0.12992437199588822, -0.9915239067019382, 8.371243355914198e-17;
0.6777699433001967, 0.08881160973099786, 0.7298906780716116)
    >>>s.trans(lp.Vector3d(1,0,0))
    Vector3d(21340.383496145718, 0.12992437179996416, -6367704.1073264275)
    >>>s.trans(lp.Vector3d(1,0,0), rotation_only=True)
    Vector3d(-0.7237040565868911, 0.12992437199588822, 0.6777699433001967)
    >>>s.trans(
            lp.Vector3d(-0.7237040565868911,
                        0.12992437199588822,
                        0.6777699433001967),
            rotation_only=True, revert=True)
    Vector3d(0.9999999999999999, -2.0816681711721685e-17, -5.551115123125783e-17)

@author: lirw (lirw@ynao.ac.cn)
"""

import math
from .constants import WGS84_EARTH_EQUATORIAL_RADIUS, WGS84_EARTH_FLATTENING
from .math3d import Vector3d, Matrix33


class Station:
    """
    Ground station

    Attributes
    ----------
    longitude : float
        longitude of the station, given in [rad].

    latitude : float
        latitude of the station, given in [rad].

    height : float
        height of the station, given in [m].

    Key Methods
    ----------
    from_position   Return a station from position(Vector3d)
    trans           Transform position between topocentric and body-fixed
    """
    def __init__(self, longitude, latitude, height, name='', station_id=0):
        self._longitude = longitude
        self._latitude = latitude
        self._height = height
        self._name = name
        self._station_id = station_id

        # compute geocentric position
        cLatitude = math.cos(latitude)
        sLatitude = math.sin(latitude)
        ae = WGS84_EARTH_EQUATORIAL_RADIUS
        f = WGS84_EARTH_FLATTENING
        g = 1.0 - f
        g2 = g * g
        e2 = f * (2.0 - f)
        xn = ae / math.sqrt(1.0 - e2 * sLatitude * sLatitude)
        r = (xn + height) * cLatitude
        px = r * math.cos(longitude)
        py = r * math.sin(longitude)
        pz = (g2 * xn + height) * sLatitude
        self._R = Vector3d(px, py, pz)

        # self._R = Vector3d(-1330021.332749, -5328403.337515, 3236481.699816)

        # compute geocentric latitude
        self._geolatitude = math.asin(pz / self._R.length)

        r3lo = Matrix33.rotation(3, longitude)
        r2la = Matrix33.rotation(2, math.pi / 2 - latitude)
        r3pi = Matrix33.rotation(3, math.pi)

        self._mlt = r3pi**(r2la**r3lo)

    @classmethod
    def from_position(cls, position):
        """
        Return a station from position(Vector3d).

        This is a class method.

        Parameters
        ----------
        position : Vector3d
            geocentric position.

        Example:
            >>>lp.Station.from_position(
                    lp.Vector3d(4331283.557, 567549.902, 4633140.353))
            7.465222 46.877230 951.331 4331283.557 567549.902 4633140.353 46.685206
        """
        ae = WGS84_EARTH_EQUATORIAL_RADIUS
        f = WGS84_EARTH_FLATTENING
        e2 = f * (2.0 - f)
        r = Vector3d(position.x, position.y, position.z)
        zi = e2 * r.z

        while True:
            # iteration
            r.z = position.z + zi
            xr = r.length
            sd = r.z / xr
            enn = ae / math.sqrt(1.0 - e2 * sd * sd)
            zi = enn * e2 * sd

            if math.fabs(zi - (r.z - position.z)) < 1.0e-8:
                break

        latitude = math.asin(sd)
        longitude = math.atan2(position.y, position.x)
        altitude = xr - enn

        return cls(longitude, latitude, altitude)

    # Read-only field accessors
    @property
    def longitude(self):
        """Return longitude, rad"""
        return self._longitude

    @property
    def geolatitude(self):
        """Return geocentric-latitude, rad"""
        return self._geolatitude

    @property
    def latitude(self):
        """Return latitude, rad"""
        return self._latitude

    @property
    def height(self):
        """Return height, m"""
        return self._height

    @property
    def name(self):
        """Return name"""
        return self._name

    @property
    def id(self):
        """return id"""
        return self._station_id

    @property
    def R(self):
        """REturn geocentric position, Vector3d, in meters."""
        return self._R

    @property
    def mlt(self):
        """Return transformation matrix from body-fixed to topocentric."""
        return self._mlt

    def __repr__(self):
        """Convert to formal string, for repr().

        >>> station = station(radians(102.7972), radians(25.0299),
                              1991.83, 7820, '1m2')
        >>> repr(station)
        '102.797434 25.029965 1993.01 -1281276.109 5640727.260 2682925.683 24.882795 1m2'
        """
        return '{:.6f} {:.6f} {:.3f} {:.3f} {:.3f} {:.3f} {:.6f} {}'.format(
                math.degrees(self._longitude), math.degrees(self._latitude),
                self._height,
                self._R.x, self._R.y, self._R.z,
                math.degrees(self._geolatitude), self._name)

    def trans(self, pos, rotation_only=False, reverse=False):
        """
        Transform position between topocentric and body-fixed.

        Parameters
        ----------
        pos : Vector3d
            position to be transformed.

        rotation_only: bool
            do rotation only if True, otherwise transform origin.

        reverse: bool
            transform from body-fixed to topocentric if True.

        Returns
        -------
        transformed position.
        """
        if reverse:  # topocentric --> body-fixed
            # step 1: revert rot
            r = self.mlt.T * pos

            # step 2: revert trans origin
            if rotation_only:
                return r
            else:  # trans origin
                return r + self.R
        else:  # body-fixed --> topocentric
            # step: trans origin
            if rotation_only:
                r = pos
            else:  # trans origin
                r = pos - self.R

            # step 2: rot
            r = self.mlt * r

            return r
