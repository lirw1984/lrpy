#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Consolidated Laser Ranging Prediction Format (CPF) v1.01

Clases:
    CPF
    A class for handling CPF file.

    H1
    Header type 1 Basic information

    H2
    Header type 2 Basic information

    H3
    Header type 3 Expected accuracy

    H4
    Header type 4 Transponder information

    H5
    Header type 5 Spherical satellite center of mass correction

    R10
    Record type 10 Position

    R30
    Record type 30 Corrections

@author: lirw (lirw@ynao.ac.cn)
"""
import os
import re
from ctypes import c_double
from datetime import datetime

import lrpy as lp
from lrpy.lagrange import lagrange
from lrpy.satellite import satellite_names, satellite_by_id


class CPF:
    """
    A class for handling CPF file.

    Attributes:
        filename   str         filename to be loaded
        h1         H1          Header 1, basic information (required)
        h2         H2          Header 2, basic information (required)
        h3         H3          Header 3, expected accuracy
        h4         H4          Header 4, transponder information
        h5         H5          Header 5, spherical satellite center of mass
                                correction

    Key Methods:
        interpolate_position    Return position at given time by lagrange
                                interpolation.
        interpolate_corrections Return corrections at given time by
                                lagrange interpolation.
    """
    def __init__(self, filename):
        # check if file exist
        if not os.path.exists(filename):
            raise FileNotFoundError(filename)

        self._filename = filename

        # split ephemeris version number and version number within the day
        name = os.path.basename(filename)
        arr = re.split(r'[_.]', name)
        if len(arr) > 3:
            version = arr[3]
        else:
            version = '0000'
        self._ephemeris_version_number = int(version[0:3])
        self._version_number_within_day = int(version[3:])

        self._h1 = None
        self._h2 = None
        self._h3 = None
        self._h4 = None
        self._h5 = None
        self._stime_mjdsod = None

        # for acc lagrange interpolation implements by c
        self._c_t = None

        self._times = []

        self._posXs = []
        self._posYs = []
        self._posZs = []

        self._velXs = []
        self._velYs = []
        self._velZx = []

        self._posXs_back = []
        self._posYs_back = []
        self._posZs_back = []

        self._corr12s = []
        self._corr23s = []
        self._aberXs = []
        self._aberYs = []
        self._aberZs = []
        self._aberXs_back = []
        self._aberYs_back = []
        self._aberZs_back = []

        with open(filename, encoding='utf-8') as f:
            for line in f:
                data = line.split(None)
                if not data:
                    # ignore empty lines
                    continue

                data[0] = data[0].upper()

                if data[0] == '99':
                    # Record type 99 Ephemeris Trailer
                    pass
                elif data[0] == 'H1':
                    # Header type 1 Basic information - 1 (required)
                    self._h1 = H1.strph1(line)
                elif data[0] == 'H2':
                    # Header type 2 Basic information - 2 (required)
                    self._h2 = H2.strph2(line)
                elif data[0] == 'H3':
                    # Header type 3 Expected accuracy
                    self._h3 = H3.strph3(line)
                elif data[0] == 'H4':
                    # Header type 4 Transponder information
                    self._h4 = H4.strph4(line)
                elif data[0] == 'H5':
                    # Header type 5 Spherical satellite
                    # center of mass correction
                    self._h5 = H5.strph5(line)
                elif data[0] == '10':
                    # Record type 10 Position
                    r10 = R10.strpr10(line)

                    # common epoch (0) or transmit(1)
                    if r10.direction_flag == 0 or r10.direction_flag == 1:
                        if self._stime_mjdsod is None:
                            self._stime_mjdsod = r10.time_mjdsod
                            tim = 0
                        else:
                            tim = self.seconds_from_start(r10.time_mjdsod)
                        self._times.append(tim)
                        self._posXs.append(r10.position_X)
                        self._posYs.append(r10.position_Y)
                        self._posZs.append(r10.position_Z)
                    else:
                        # receive(2)
                        self._posXs_back.append(r10.position_X)
                        self._posYs_back.append(r10.position_Y)
                        self._posZs_back.append(r10.position_Z)

                    # print('10-{}: {}'.format(dirflag, line))
                elif data[0] == '20':
                    # Record type 20 Velocity
                    print('20: ' + line)
                elif data[0] == '30':
                    # Record type 30 Corrections
                    # (All targets computed from a solar system ephemeris)
                    r30 = R30.strpr30(line)

                    # common epoch (0) or transmit(1)
                    if r30.direction_flag == 0 or r30.direction_flag == 1:
                        self._aberXs.append(r30.aberration_correction_X)
                        self._aberYs.append(r30.aberration_correction_Y)
                        self._aberZs.append(r30.aberration_correction_Z)

                        self._corr12s.append(r30.relativistic_range_correction)
                    else:
                        # receive(2)
                        self._aberXs_back.append(r30.aberration_correction_X)
                        self._aberYs_back.append(r30.aberration_correction_Y)
                        self._aberZs_back.append(r30.aberration_correction_Z)

                        self._corr23s.append(r30.relativistic_range_correction)
                elif data[0] == '40':
                    # Record type 40 Transponder specific (Transponders)
                    print('40: ' + line)
                elif data[0] == '50':
                    # Record type 50 Offset from center of main body
                    # (Surface features and satellites)
                    print('50: ' + line)
                elif data[0] == '60':
                    # Record type 60 Rotation angle of offset
                    # (Surface features)'''
                    print('60: ' + line)
                elif data[0] == '70':
                    # Record type 70 Earth orientation
                    # (For space-fixed mode, as needed, typically once a day)
                    print('70: ' + line)
                elif data[0] == '00':
                    # Comments
                    print('00: ' + line)

    def seconds_from_start(self, mjdsod):
        """Return seconds since cpf start datetime, in seconds."""
        return (mjdsod[0] - self._stime_mjdsod[0]) * 86400.0 \
            + (mjdsod[1] - self._stime_mjdsod[1])

    def interpolate_position(self, t, backleg=False):
        """
        Return position at given time by lagrange interpolation.

        Parameters
        ----------
        t: (mjd, sod) or datetime
            time to interpolating position. It can be (mjd, sod) or a datetime.
            If datetime is given, UTC is assumed.

        backleg: bool
            if False, for outleg, otherwise for backleg.

        Returns
        ----------
        interpolated position, (px, py, pz), in meters.
        """
        if type(t) is datetime:
            # datetime --> (mjd, sod)
            (mjd, sod) = lp.mjd_sod(datetime)
            t = (mjd, sod)

        tim = self.seconds_from_start(t)

        count = len(self._times)

        if self._c_t is None:
            c_array_type = c_double * count
            self._c_t = c_array_type()
            for i in range(0, count):
                self._c_t[i] = self._times[i]

        if not backleg:
            pxyz = lagrange(
                    tim, 10, self._c_t,
                    [self._posXs, self._posYs, self._posZs])
            px, py, pz = (pxyz[0], pxyz[1], pxyz[2])
        else:
            if self._posXs_back:
                pxyz = lagrange(
                    tim, 10, self._c_t,
                    [self._posXs_back, self._posYs_back, self._posZs_back])
                px, py, pz = (pxyz[0], pxyz[1], pxyz[2])
            else:
                pxyz = lagrange(
                    tim, 10, self._c_t,
                    [self._posXs, self._posYs, self._posZs])
                px, py, pz = (-pxyz[0], -pxyz[1], -pxyz[2])

        return (px, py, pz)

    def interpolate_corrections(self, t, backleg=False):
        """
        Return corrections at given time by lagrange interpolation.

        The corrections are stellar aberration correction and
        Relativistic range correction.

        Parameters
        ----------
        t: (mjd, sod) or datetime
            time to interpolating position. It can be (mjd, sod) or a datetime.
            If datetime is given, UTC is assumed.

        backleg: bool
            if False, for outleg, otherwise for backleg.

        Returns
        ----------
        interpolated corrections, (abx, aby, abz, corr),
        aberations correction in meters, range correction in nsec.
        """
        tim = self.seconds_from_start(t)

        count = len(self._times)

        if self._c_t is None:
            c_array_type = c_double * count
            self._c_t = c_array_type()
            for i in range(0, count):
                self._c_t[i] = self._times[i]

        if not backleg:
            abxyz_cr = lagrange(
                    tim, 10, self._c_t,
                    [self._aberXs, self._aberYs, self._aberZs, self._corr12s])
            abx, aby, abz, corr = (abxyz_cr[0], abxyz_cr[1], abxyz_cr[2],
                                   abxyz_cr[3])
        else:
            if self._aberXs_back:
                abxyz_cr = lagrange(
                    tim, 10, self._c_t,
                    [self._aberXs_back, self._aberYs_back, self._aberZs_back,
                     self._corr23s])
                abx, aby, abz, corr = (abxyz_cr[0], abxyz_cr[1], abxyz_cr[2],
                                       abxyz_cr[3])
            else:
                abxyz_cr = lagrange(
                    tim, 10, self._c_t,
                    [self._aberXs, self._aberYs, self._aberZs, self._corr12s])
            abx, aby, abz, corr = (-abxyz_cr[0], -abxyz_cr[1], -abxyz_cr[2],
                                   -abxyz_cr[3])

        return (abx, aby, abz, corr)

    # Read-only field accessors
    @property
    def filename(self):
        """Return the filename."""
        return self._filename

    @property
    def ephemeris_version_numer(self):
        """Return the ephemeris version number."""
        return self._ephemeris_version_number

    @property
    def version_number_within_day(self):
        """Return the version number within the day."""
        return self._version_number_within_day

    @property
    def format_version(self):
        """Return format version."""
        return self._h1.format_version

    @property
    def start_datetime(self):
        """Return start datetime of cpf defined in H2, datetime."""
        return self._h2.start_datetime

    @property
    def end_datetime(self):
        """Return end datetime of cpf defined in H2, datetime."""
        return self._h2.end_datetime

    @property
    def time_interval(self):
        """Return time interval between table entries, seconds."""
        return self._h2.time_interval

    @property
    def target_name(self):
        """Return target name from official list (e.g. lageos1)."""
        return self._h1.target_name

    @property
    def target_id(self):
        """Return target id (COSPAR ID)(e.g. '7603901' for lageos1)."""
        return self._h2.satellite['satellite_id']

    @property
    def target_ext(self):
        """Return ext of target (short name)(e.g. 'la1' for lageos1)."""
        return self._h2.satellite['ext']

    @property
    def norad(self):
        """Return norad number of target (e.g. '8820' for lageos1)."""
        return self._h2.satellite['norad_number']

    @property
    def target_type(self):
        """Return type of target defined in H2."""
        return self._h2.target_type

    @property
    def h1(self):
        """Return header type 1 - Basic information."""
        return self._h1

    @property
    def h2(self):
        """Return header type 2 - Basic information."""
        return self._h2

    @property
    def h3(self):
        """Return header type 3 - Expected accuracy. It may be None."""
        return self._h3

    @property
    def h4(self):
        """Return Header type 4 - Transponder information. It may be None."""
        return self._h4

    @property
    def h5(self):
        """Return Header type 5 - Spherical satellite center of mass
        correction. It may be None.
        """
        return self._h5


class H1:
    """
    Header type 1 Basic information - 1 (required)

    +-------+-----+-----------------------------------------------------------+
    | ind   | fmt | descriptions                                              |
                   --> valid check                          | Error           |
    +=======+=====+=========================================+=================+
    | 1-2   | A2  | Record Type (='H1')                                       |
                   --> ['H1']                               | Exxxxx          |
    +-------+-----+-----------------------------------------+-----------------+
    | 4-6   | A3  | Consolidated Prediction Format (='CPF')                   |
                   --> ['CPF']                              | EH1021          |
    +-------+-----+-----------------------------------------+-----------------+
    | 8-9   | I2  | Format Version                                            |
    +-------+-----+-----------------------------------------------------------+
    | 12-14 | A3  | Ephemeris Source (e.g., 'HON', 'UTX')                     |
    +-------+-----+-----------------------------------------------------------+
    | 16-19 | I4  | Year of ephemeris production                              |
                   --> [1950,...,2100]                      | EH1051          |
    +-------+-----+-----------------------------------------+-----------------+
    | 21-22 | I2  | Month of ephemeris production                             |
                   --> [1,...,12]                           | EH1061          |
    +-------+-----+-----------------------------------------+-----------------+
    | 24-25 | I2  | Day of ephemeris production                               |
                   --> [1,...31]                            | EH1071          |
    +-------+-----+-----------------------------------------+-----------------+
    | 27-28 | I2  | Hour of ephemeris production (UTC)                        |
                   --> [0,...,23]                           | EH1081          |
    +-------+-----+-----------------------------------------+-----------------+
    | 31-34 | I4  | Ephemeris Sequence number                                 |
                   --> [5000,...,9000]                      | EH1091          |
    +-------+-----+-----------------------------------------+-----------------+
    | 36-45 | A10 | Target name from official list (e.g. lageos1)             |
                   --> exists in official list              | EH1061          |
    +-------+-----+-----------------------------------------+-----------------+
    | 47-56 | A10 | Notes (e.g., '041202','DE-403')                           |
    +-------+-----+-----------------------------------------------------------+

    Additional Checks:
                --> The record length must be 54 characters            | EH1001
                --> Date of file production must be valid              | EH1002
                --> Wrong pattern of record (spaces at wrong positions)| EH1003

    exams: 'H1 CPF  1  SGF 2018  2  5  2  5361 ajisai               '
           'H1 CPF  1  UTX 2006 12 21 15  8551 apollo15   jpl_de-403'
           >>>h1 = H1(1,'sgf',datetime(2018,2,5,2),5361,'ajisai')
           >>>repr(h1)
               'H1 CPF  1 SGF 2018 02 05 02 5361 ajisai               '
           >>>h1 = H1(1,'utx',datetime(2006,12,21,15),8551,'apollo15','jpl_de-403')
           >>>repr(h1)
              'H1 CPF  1 UTX 2006 12 21 15 8551 apollo15   jpl_de-403'
    """
    def __init__(self,
                 format_version,
                 ephemeris_source,
                 production_datetime,
                 ephemeris_seq_num,
                 target_name,
                 notes=''):
        if format_version is None:
            self._format_version = format_version
        else:  # default is 1
            self._format_version = 1

        self._ephemeris_source = ephemeris_source.upper()
        self._production_datetime = production_datetime
        self._ephemeris_seq_num = ephemeris_seq_num
        self._target_name = target_name
        self._notes = notes

    @classmethod
    def strph1(cls, h1_string):
        """string --> new H1 parsed from a string."""
        data = h1_string.split(None)
        errors = []

        if data[0].upper() != 'H1':
            errors.append('Exxxxx')

        if data[1].upper() != 'CPF':
            errors.append('EH1021')

        format_version = int(data[2])
        ephemeris_source = data[3]

        try:
            year = int(data[4])
        except ValueError:
            year = -1

        if not (1950 <= year <= 2100):
            errors.append('EH1051')

        try:
            month = int(data[5])
            day = int(data[6])
            hour = int(data[7])
            production_datetime = datetime(year, month, day, hour)
        except ValueError:
            errors.insert(-1, 'EH1002')
            if not (1 <= month <= 12):
                errors.append('EH1061')

            if not (1 <= day <= 31):
                errors.append('EH1071')

            if not (1 <= hour <= 23):
                errors.append('EH1081')

        try:
            ephemeris_seq_num = int(data[8])
        except ValueError:
            ephemeris_seq_num = 0

        if not (5000 <= ephemeris_seq_num <= 9000):
            errors.append('EH1091')

        target_name = data[9]
        if (satellite_names is not None) and \
           (target_name not in satellite_names):
            # errors.append('EH1111')
            pass

        if len(data) >= 11:
            notes = data[10]
        else:
            notes = ''

        if errors:
            raise ValueError(errors)

        return cls(format_version, ephemeris_source, production_datetime,
                   ephemeris_seq_num, target_name, notes)

    def __repr__(self):
        return '{} {} {:>2} {:>3} {:>13} {:>4} {:<10} {:>10}'.format(
            'H1', 'CPF', self.format_version, self._ephemeris_source,
            self._production_datetime.strftime('%Y %m %d %H'),
            self._ephemeris_seq_num, self._target_name, self._notes)

    def __str__(self):
        return '{} {} {} {} {}'.format(
            self._target_name, self._ephemeris_seq_num, self._ephemeris_source,
            self._production_datetime.strftime('%Y-%m-%dT%H'), self._notes)

    # Read-only field accessors
    @property
    def format_version(self):
        """Return format version, int. It is mostly 1."""
        return self._format_version

    @property
    def ephemeris_source(self):
        """Return ephemeris source (e.g., 'HON', 'UTX')."""
        return self._ephemeris_source

    @property
    def production_datetime(self):
        """Return ephemeris production datetime."""
        return self._production_datetime

    @property
    def ephemeris_seq_num(self):
        """Return ephemeris sequence number [5000,...,9000]."""
        return self._ephemeris_seq_num

    @property
    def target_name(self):
        """Return target name from official list (e.g. lageos1)."""
        return self._target_name

    @property
    def notes(self):
        """Return notes (e.g., '041202','DE-403'). It is normaly empty."""
        return self._notes


class H2:
    """
    Header type 2 Basic information - 2 (required)

    +-------+-----+-----------------------------------------------------------+
    | ind   | fmt | descriptions                                              |
                   --> valid check                          |  Error          |
    +=======+=====+=========================================+=================+
    | 1-2   | A2  | Record Type (='H2')                                       |
                   --> ['H2']                               | Exxxxx          |
    +-------+-----+-----------------------------------------+-----------------+
    | 4-11  | I8  | COSPAR ID                                                 |
    +-------+-----+-----------------------------------------------------------+
    | 13-16 | I4  | SIC                                                       |
    +-------+-----+-----------------------------------------------------------+
    | 18-25 | I8  | NORAD ID                                                  |
    +-------+-----+-----------------------------------------------------------+
    | 27-30 | I4  | Starting Year                                             |
                   --> [1950,...,2100]                      | EH2051          |
    +-------+-----+-----------------------------------------+-----------------+
    | 32-33 | I2  | Starting Month                                            |
                   --> [1,...,12]                           | EH2061          |
    +-------+-----+-----------------------------------------+-----------------+
    | 35-36 | I2  | Starting Day                                              |
                   --> [1,...31]                            | EH2071          |
    +-------+-----+-----------------------------------------+-----------------+
    | 38-39 | I2  | Starting Hour (UTC)                                       |
                   --> [0,...,23]                           | EH2081          |
    +-------+-----+-----------------------------------------+-----------------+
    | 41-42 | I2  | Starting Minute (UTC)                                     |
                   --> [0,...,59]                           | EH2091          |
    +-------+-----+-----------------------------------------+-----------------+
    | 44-45 | I2  | Starting Second (UTC)                                     |
                   --> [0,...,59]                           | EH2101          |
    +-------+-----+-----------------------------------------+-----------------+
    | 47-50 | I4  | Ending Year                                               |
                   --> [1950,...,2100]                      | EH2111          |
    +-------+-----+-----------------------------------------+-----------------+
    | 52-53 | I2  | Ending Month                                              |
                   --> [1,...,12]                           | EH2121          |
    +-------+-----+-----------------------------------------+-----------------+
    | 55-56 | I2  | Ending Day                                                |
                   --> [1,...31]                            | EH2131          |
    +-------+-----+-----------------------------------------+-----------------+
    | 58-59 | I2  | Ending Hour (UTC)                                         |
                   --> [0,...,23]                           | EH2141          |
    +-------+-----+-----------------------------------------+-----------------+
    | 61-62 | I2  | Ending Minute (UTC)                                       |
                   --> [0,...,59]                           | EH2151          |
    +-------+-----+-----------------------------------------+-----------------+
    | 64-65 | I2  | Ending Second (UTC)                                       |
                   --> [0,...,59]                           | EH2161          |
    +-------+-----+-----------------------------------------+-----------------+
    | 67-71 | I5  | Time between table entries (UTC seconds)(=0 if variable)  |
    +-------+-----+-----------------------------------------------------------+
    | 73    | I1  | Compatibility with TIVs = 1
                    (=> integrable, geocentric ephemeris)                     |
    +-------+-----+-----------------------------------------------------------+
    | 75    | I1  | Target type                                               |
                    1=passive (retro-reflector) artificial satellite          |
                    2=passive (retro-reflector) lunar reflector               |
                    3=synchronous transponder                                 |
                    4=asynchronous transponder                                |
                    --> [1,2,3,4]                           | EH2191          |
    +-------+-----+-----------------------------------------+-----------------+
    | 77-78 | I2  | Reference frame                                           |
                    0=geocentric true body fixed (default)                    |
                    1=geocentric space fixed (i.e. Inertial) (True of Date)   |
                    2=geocentric space fixed (Mean of Date J2000)             |
                    --> [0,1,2]                             | EH2201          |
    +-------+-----+-----------------------------------------+-----------------+
    | 80    | I1  | Rotational angle type                                     |
                    0= Not Applicable                                         |
                    1= Lunar Euler angles: f, q, and y                        |
                    2= North pole Right Ascension and Declination,            |
                       and angle to prime meridian (a0, d0, and W)            |
                    --> [0,1,2]                             | EH2211          |
    +-------+-----+-----------------------------------------+-----------------+
    | 82    | I1  | Center of mass correction                                 |
                    0= None applied.Prediction is for center of mass of target|
                    1= Applied. Prediction is for retro-reflector array       |
                    --> [0,1]                               | EH2221          |
    +-------+-----+-----------------------------------------+-----------------+

    Additional Checks:
                --> The record length must be 82 characters            | EH2001
                --> Starting date must be valid                        | EH2002
                --> Ending date must be valid                          | EH2003
                --> Wrong pattern of record (spaces at wrong positions)| EH2004

    exams:
        'H2  8606101 1500    16908 2018  2  4  0  0  0 2018  2  9 23 55  0   240 1 1  0 0 0'
        'H2      103  103        0 2006 12 20  0  0  0 2006 12 20 23 45  0   900 0 2  0 0 0'
    """
    TargetTypes = [
            '',
            'passive (retro-reflector) artificial satellite',
            'passive (retro-reflector) lunar reflector',
            'synchronous transponder',
            'asynchronous transponder'
            ]
    ReferenceFrames = [
            'geocentric true body fixed (default)',
            'geocentric space fixed (i.e. Inertial) (True of Date)',
            'geocentric space fixed (Mean of Date J2000)'
            ]
    RotationalAngleTypes = [
            'Not Applicable',
            'Lunar Euler angles: f, q, and y',
            'North pole Right Ascension and Declination, \
            and angle to prime meridian (a0, d0, and W)'
            ]
    CenterOfMassCorrections = ['None applied', 'Applied']

    def __init__(self, satellite, starting_datetime, ending_datetime,
                 time_interval, compatibility_with_TIVs, target_type,
                 reference_frame, rotational_angle_type,
                 center_of_mass_correction):
        self._satellite = satellite
        self._starting_datetime = starting_datetime
        self._ending_datetime = ending_datetime
        self._time_interval = time_interval
        self._compatibility_with_TIVs = compatibility_with_TIVs
        self._target_type = target_type
        self._reference_frame = reference_frame
        self._rotational_angle_type = rotational_angle_type
        self._center_of_mass_correction = center_of_mass_correction

    @classmethod
    def strph2(cls, h2_string):
        """string --> new H2 parsed from a string."""
        data = h2_string.split(None)
        errors = []

        if data[0].upper() != 'H2':
            errors.append('Exxxxx')

        try:
            satellite_id = int(data[1])
            sic_code = int(data[2])
            try:
                norad_number = int(data[3])
            except ValueError:
                norad_number = 0

            ilrs_sat = satellite_by_id(satellite_id)
            if ilrs_sat is None:
                ext = '{:06d}'.format(norad_number)
            else:
                ext = ilrs_sat['ext']

            satellite = {
                'satellite_id': satellite_id,
                'sic_code': sic_code,
                'norad_number': norad_number,
                'ext': ext
            }

            if (satellite_id > 1000) and (norad_number == 0):
                errors.append('EH2042')
        except ValueError:
            pass

        try:
            starting_year = int(data[4])
        except ValueError:
            starting_year = -1

        if not (1950 <= starting_year <= 2100):
            errors.append('EH2051')

        try:
            starting_month = int(data[5])
            starting_day = int(data[6])
            starting_hour = int(data[7])
            starting_minute = int(data[8])
            starting_second = int(data[9])
            starting_datetime = datetime(starting_year, starting_month,
                                         starting_day, starting_hour,
                                         starting_minute, starting_second)
        except ValueError:
            errors.insert(-1, 'EH2002')
            if not (1 <= starting_month <= 12):
                errors.append('EH2061')

            if not (1 <= starting_day <= 31):
                errors.append('EH2071')

            if not (0 <= starting_hour <= 23):
                errors.append('EH2081')

            if not (0 <= starting_minute <= 59):
                errors.append('EH2091')

            if not (0 <= starting_second <= 59):
                errors.append('EH2101')

        try:
            ending_year = int(data[10])
        except ValueError:
            ending_year = -1

        if not (1950 <= ending_year <= 2100):
            errors.append('EH2111')

        try:
            ending_month = int(data[11])
            ending_day = int(data[12])
            ending_hour = int(data[13])
            ending_minute = int(data[14])
            ending_second = int(data[15])
            ending_datetime = datetime(ending_year, ending_month, ending_day,
                                       ending_hour, ending_minute,
                                       ending_second)
        except ValueError:
            errors.insert(-1, 'EH2003')
            if not (1 <= ending_month <= 12):
                errors.append('EH2121')

            if not (1 <= ending_day <= 31):
                errors.append('EH2131')

            if not (1 <= ending_hour <= 23):
                errors.append('EH2141')

            if not (0 <= ending_minute <= 59):
                errors.append('EH2151')

            if not (0 <= ending_second <= 59):
                errors.append('EH2161')

        time_interval = int(data[16])

        compatibility_with_TIVs = int(data[17])

        try:
            target_type = int(data[18])
        except ValueError:
            target_type = -1

        if not (1 <= target_type <= 4):
            errors.append('EH2191')

        try:
            reference_frame = int(data[19])
        except ValueError:
            reference_frame = -1

        if not (0 <= reference_frame <= 2):
            errors.append('EH2201')

        try:
            rotational_angle_type = int(data[20])
        except ValueError:
            rotational_angle_type = -1

        if not (0 <= rotational_angle_type <= 2):
            errors.append('EH2211')

        try:
            center_of_mass_correction = int(data[21])
        except ValueError:
            center_of_mass_correction = -1

        if not (0 <= center_of_mass_correction <= 1):
            errors.append('EH2221')

        if errors:
            raise ValueError(errors)

        return cls(satellite, starting_datetime, ending_datetime,
                   time_interval, compatibility_with_TIVs, target_type,
                   reference_frame, rotational_angle_type,
                   center_of_mass_correction)

    def __repr__(self):
        return '{}  {:>07} {:>04} {:>8} {} {} {:>5} {} {} {:>2} {} {}'.format(
            'H2', self._satellite['satellite_id'], self._satellite['sic_code'],
            self._satellite['norad_number'],
            self._starting_datetime.strftime('%Y %m %d %H %M %S'),
            self._ending_datetime.strftime('%Y %m %d %H %M %S'),
            self._time_interval, self._compatibility_with_TIVs,
            self._target_type, self._reference_frame,
            self._rotational_angle_type, self._center_of_mass_correction)

    def __str__(self):
        s1 = 'statllite: {}({}) {}'.format(self._satellite['satellite_id'],
                                           self._satellite['sic_code'],
                                           self._satellite['norad_number'])
        s2 = 'Epoch: {} ~ {}'.format(
            self._starting_datetime.strftime('%Y-%m-%dT%H:%M:%S'),
            self._ending_datetime.strftime('%Y-%m-%dT%H:%M:%S'))
        s3 = 'Interval(secs): {}'.format(self._time_interval)
        s4 = 'TargetType: {} {}'.format(self._target_type,
                                        H2.TargetTypes[self._target_type])
        s5 = 'ReferenceFrame: {} {}'.format(
            self._reference_frame, H2.ReferenceFrames[self._reference_frame])
        s6 = 'RotationalAngleType: {} {}'.format(
            self._rotational_angle_type,
            H2.RotationalAngleTypes[self._rotational_angle_type])
        s7 = 'CenterOfMassCorrection: {} {}'.format(
            self._center_of_mass_correction,
            H2.CenterOfMassCorrections[self._center_of_mass_correction])

        return '{}\n{}\n{}\n{}\n{}\n{}\n{}'.format(s1, s2, s3, s4, s5, s6, s7)

    # Read-only field accessors
    @property
    def satellite(self):
        """Return satellite in json format (dict).
        {'satellite_id': COSPAR ID, 'sic_code': SIC, 'norad_number': NORAD ID}
        """
        return self._satellite

    @property
    def start_datetime(self):
        """Return ephemeris starting datetime, datetime."""
        return self._starting_datetime

    @property
    def end_datetime(self):
        """Return ephemeris ending datetime, datetime."""
        return self._ending_datetime

    @property
    def time_interval(self):
        """Return time interval between table entries, in seconds."""
        return self._time_interval

    @property
    def target_type(self):
        """
        Return target type, int.

        Target type is defined as (v1.01):
            1=passive (retro-reflector) artificial satellite
            2=passive (retro-reflector) lunar reflector
            3=synchronous transponder
            4=asynchronous transponder
        """
        return self._target_type

    @property
    def reference_frame(self):
        """
        Return reference frame, int.

        Reference frame is defined as:
            0=geocentric true body fixed (default)
            1=geocentric space fixed (i.e. Inertial) (True of Date)
            2=geocentric space fixed (Mean of Date J2000)
        """
        return self._reference_frame

    @property
    def rotational_angle_type(self):
        """
        Return rotational angle type, int.

        Rotational angle type is defined as:
            0= Not Applicable
            1= Lunar Euler angles: f, q, and y
            2= North pole Right Ascension and Declination,
                and angle to prime meridian (a0, d0, and W)
        """
        return self._rotational_angle_type

    @property
    def center_of_mass_correction_applied(self):
        """Return if center of mass_correction is applied or not, bool."""
        # 0=None applied, 1=Applied
        return self._center_of_mass_correction == 1


class H3:
    """
    Header type 3 Expected accuracy

    +-------+-----+-----------------------------------------------------------+
    | ind   | fmt | descriptions                                              |
                   --> valid check                          | Error           |
    +=======+=====+=========================================+=================+
    | 1-2   | A2  | Record Type (='H3')                                       |
                   --> ['H3']                               | Exxxxx          |
    +-------+-----+-----------------------------------------+-----------------+
    | 4-8   | I5  | Along-track run-off after 0 hours (meters)                |
    +-------+-----+-----------------------------------------------------------+
    | 10-14 | I5  | Cross-track run-off after 0 hours (meters)                |
    +-------+-----+-----------------------------------------------------------+
    | 15-20 | I5  | Radial run-off after 0 hours (meters)                     |
    +-------+-----+-----------------------------------------------------------+
    | 22-26 | I5  | Along-track run-off after 6 hours (meters)                |
    +-------+-----+-----------------------------------------------------------+
    | 28-32 | I5  | Cross-track run-off after 6 hours (meters)                |
    +-------+-----+-----------------------------------------------------------+
    | 34-38 | I5  | Radial run-off after 6 hours (meters)                     |
    +-------+-----+-----------------------------------------------------------+
    | 40-44 | I5  | Along-track run-off after 24 hours (meters)               |
    +-------+-----+-----------------------------------------------------------+
    | 46-50 | I5  | Cross-track run-off after 24 hours (meters)               |
    +-------+-----+-----------------------------------------------------------+
    | 52-56 | I5  | Radial run-off after 24 hours (meters)                    |
    +-------+-----+-----------------------------------------------------------+

    Additional Checks:
                --> The record length must be 56 characters            | EH3001

    exams:

    """
    def __init__(self):
        pass

    @classmethod
    def strph3(cls, h3_string):
        """string --> new H3 parsed from a string."""
        data = h3_string.split(None)
        errors = []

        if data[0].upper() != 'H3':
            errors.append('Exxxxx')

        if errors:
            raise ValueError(errors)

        return cls()

    def __repr__(self):
        return 'H3'

    def __str__(self):
        return ''

    # Read-only field accessors


class H4:
    """
    Header type 4 Transponder information

    +-------+-----+-----------------------------------------------------------+
    | ind   | fmt | descriptions                                              |
                   --> valid check                          | Error           |
    +=======+=====+=========================================+=================+
    | 1-2   | A2  | Record Type (='H4')                                       |
                   --> ['H4']                               | Exxxxx          |
    +-------+-----+-----------------------------------------------------------+
    |  4-15 |F12.5| Pulse Repetition Frequency (PRF) in Hz                    |
    +-------+-----+-----------------------------------------------------------+
    | 17-26 |F10.4| Transponder transmit delay in microseconds                |
    +-------+-----+-----------------------------------------------------------+
    | 28-38 |F11.2| Transponder UTC offset in microseconds                    |
    +-------+-----+-----------------------------------------------------------+
    | 40-50 |F11.2| Transponder Oscillator Drift in parts in 10^15            |
    +-------+-----+-----------------------------------------------------------+

    Additional Checks:
                --> The record length must be 50 characters            | EH3001

    exams:

    """
    def __init__(self):
        pass

    @classmethod
    def strph4(cls, h4_string):
        """string --> new H4 parsed from a string."""
        data = h4_string.split(None)
        errors = []

        if data[0].upper() != 'H4':
            errors.append('Exxxxx')

        if errors:
            raise ValueError(errors)

        return cls()

    def __repr__(self):
        return 'H4'

    def __str__(self):
        return ''

    # Read-only field accessors


class H5:
    """
    Header type 5 Spherical satellite center of mass correction

    +-------+-----+-----------------------------------------------------------+
    | ind   | fmt | descriptions                                              |
                   --> valid check                          | Error           |
    +=======+=====+=========================================+=================+
    | 1-2   | A2  | Record Type (='H5')                                       |
                   --> ['H5']                               | Exxxxx          |
    +-------+-----+-----------------------------------------------------------+
    |  4-15 | F7.4| Approximate center of mass to reflector offset in meters  |
                    (always positive)                                         |
    +-------+-----+-----------------------------------------------------------+

    Additional Checks:
                --> The record length must be 10 characters            | EH3001

    exams:

    """
    def __init__(self, approx_center_of_mass):
        self._approx_center_of_mass = approx_center_of_mass

    @classmethod
    def strph5(cls, h5_string):
        """string --> new H5 parsed from a string."""
        data = h5_string.split(None)
        errors = []

        if data[0].upper() != 'H5':
            errors.append('Exxxxx')

        try:
            approx_center_of_mass = float(data[1])
        except ValueError:
            approx_center_of_mass = 0

        if errors:
            raise ValueError(errors)

        return cls(approx_center_of_mass)

    def __repr__(self):
        return '{} {:>7.4f}'.format('H5', self._approx_center_of_mass)

    def __str__(self):
        return '{:>7.4f}'.format(self._approx_center_of_mass)

    # Read-only field accessors
    @property
    def approx_center_of_mass(self):
        """
        Return approximate center of mass to reflector offset in meters
        (always positive)
        """
        return self._approx_center_of_mass


class R10:
    """
    Record type 10 Position

    +-------+-----+-----------------------------------------------------------+
    | ind   | fmt | descriptions                                              |
                   --> valid check                          | Error           |
    +=======+=====+=========================================+=================+
    | 1-2   | A2  | Record Type (='10')                                       |
                   --> ['10']                               | Exxxxx          |
    +-------+-----+-----------------------------------------------------------+
    |       | I1  | Direction flag (common epoch=0; transmit=1; receive=2)    |
    +-------+-----+-----------------------------------------------------------+
    |       | I5  | Modified Julian Date (MJD)                                |
    +-------+-----+-----------------------------------------------------------+
    |       |F13.6| Seconds of Day (UTC) (Transmit or receive)                |
    +-------+-----+-----------------------------------------------------------+
    |       | I2  | Leap second flag (= 0 or the value of the new leap second)|
    +-------+-----+-----------------------------------------------------------+
    |       |F17.3| Geocentric X position in meters                           |
    +-------+-----+-----------------------------------------------------------+
    |       |F17.3| Geocentric Y position in meters                           |
    +-------+-----+-----------------------------------------------------------+
    |       |F17.3| Geocentric Z position in meters                           |
    +-------+-----+-----------------------------------------------------------+

    Additional Checks:
                --> The record length must be 8 characters          | EH10001

    exams:
    '10 0 58153      0.00000  0  -5746101.109  -5325187.157    784204.161'
    '10 1 54089     0.0 0     -335984448.176       52241037.708     -180972732.287'
    '10 2 54089     0.0 0      335974813.850      -52226162.353      180973713.640'
    """
    def __init__(self, direction_flag, mjd, sod, leap_second_flag,
                 position_X, position_Y, position_Z):
        self._direction_flag = direction_flag
        self._mjd = mjd
        self._sod = sod
        self._leap_second_flag = leap_second_flag
        self._position_X = position_X
        self._position_Y = position_Y
        self._position_Z = position_Z

    @classmethod
    def strpr10(cls, r10_string):
        """string --> new R10 parsed from a string."""
        data = r10_string.split(None)
        errors = []

        if data[0] != '10':
            errors.append('Exxxxx')

        try:
            direction_flag = int(data[1])
            mjd = int(data[2])
            sod = float(data[3])
            leap_second_flag = int(data[4])
            position_X = float(data[5])
            position_Y = float(data[6])
            position_Z = float(data[7])
        except ValueError:
            pass

        if errors:
            raise ValueError(errors)

        return cls(direction_flag, mjd, sod, leap_second_flag,
                   position_X, position_Y, position_Z)

    def __repr__(self):
        return '{} {} {:>5} {:>13.6f} {:>2} {:>17.3f} {:>17.3f} \
                {:>17.3f}'.format(
                '10', self._direction_flag, self._mjd, self._sod,
                self._leap_second_flag,
                self._position_X, self._position_Y, self._position_Z)

    def __str__(self):
        return ''

    # Read-only field accessors
    @property
    def time_mjdsod(self):
        """Return epoch, in the format (mjd, sod)."""
        return (self._mjd, self._sod)

    @property
    def direction_flag(self):
        """
        Return direction flag, int.

        Direction flag is defined as:
            0=commom epoch
            1=transmit
            2=receive
        """
        return self._direction_flag

    @property
    def position_X(self):
        """Return geocentric X position in meters."""
        return self._position_X

    @property
    def position_Y(self):
        """Return geocentric Y position in meters."""
        return self._position_Y

    @property
    def position_Z(self):
        """Return geocentric Z position in meters."""
        return self._position_Z


class R30:
    """
    Record type 30 Corrections
    (All targets computed from a solar system ephemeris)

    +-------+-----+-----------------------------------------------------------+
    | ind   | fmt | descriptions                                              |
                   --> valid check                          | Error           |
    +=======+=====+=========================================+=================+
    | 1-2   | A2  | Record Type (='30')                                       |
                   --> ['30']                               | Exxxxx          |
    +-------+-----+-----------------------------------------------------------+
    |       | I1  | Direction flag (common epoch=0; transmit=1; receive=2)    |
    +-------+-----+-----------------------------------------------------------+
    |       |F18.6| X stellar aberration correction in meters                 |
    +-------+-----+-----------------------------------------------------------+
    |       |F18.6| Y stellar aberration correction in meters                 |
    +-------+-----+-----------------------------------------------------------+
    |       |F18.6| Z stellar aberration correction in meters                 |
    +-------+-----+-----------------------------------------------------------+
    |       |F15.1| Relativistic range correction in nsec (positive number)   |
    +-------+-----+-----------------------------------------------------------+

    Additional Checks:
                --> The record length must be 8 characters          | EH10001

    exams: '30 1   -4422. -38240.  -2830.  25.7'

    """
    def __init__(self, direction_flag, aberration_correction_X,
                 aberration_correction_Y, aberration_correction_Z,
                 relativistic_range_correction):
        self._direction_flag = direction_flag
        self._aberration_correction_X = aberration_correction_X
        self._aberration_correction_Y = aberration_correction_Y
        self._aberration_correction_Z = aberration_correction_Z
        self._relativistic_range_correction = relativistic_range_correction

    @classmethod
    def strpr30(cls, r30_string):
        """string --> new R30 parsed from a string."""
        data = r30_string.split(None)
        errors = []

        if data[0].upper() != '30':
            errors.append('Exxxxx')

        try:
            direction_flag = int(data[1])
            aberration_correction_X = float(data[2])
            aberration_correction_Y = float(data[3])
            aberration_correction_Z = float(data[4])
            relativistic_range_correction = float(data[5])
        except ValueError:
            pass

        if errors:
            raise ValueError(errors)

        return cls(direction_flag, aberration_correction_X,
                   aberration_correction_Y, aberration_correction_Z,
                   relativistic_range_correction)

    def __repr__(self):
        return '{} {} {:18.6f} {:18.6f} {:18.6f} {:15.1f}'.format(
            '30', self._direction_flag, self._aberration_correction_X,
            self._aberration_correction_Y, self._aberration_correction_Z,
            self._relativistic_range_correction)

    def __str__(self):
        return ''

    # Read-only field accessors
    @property
    def direction_flag(self):
        """
        Return direction flag, int.

        Direction flag is defined as:
            0=commom epoch
            1=transmit
            2=receive
        """
        return self._direction_flag

    @property
    def aberration_correction_X(self):
        """Return X stellar aberration correction in meters."""
        return self._aberration_correction_X

    @property
    def aberration_correction_Y(self):
        """Return Y stellar aberration correction in meters."""
        return self._aberration_correction_Y

    @property
    def aberration_correction_Z(self):
        """Return Z stellar aberration correction in meters."""
        return self._aberration_correction_Z

    @property
    def relativistic_range_correction(self):
        """Return relativistic range correction in nsec."""
        return self._relativistic_range_correction
