#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Util for list cpf filenames, the absolute path for a given cpf filename, etc.

The directories structure is refer to ftp server's structure, that is:

    cpf_predicts
        2018
            ajisai
                ajisai_cpf_181101_8051.jax
            beaconc
                beaconc_cpf_181101_8051.sgf

Functions:
    list_cpf_filenames      Return a list of cpf file names (absolute path)
    cpf_absolute_filename   Return the absolute path for a given cpf filename

@author: lirw (lirw@ynao.ac.cn)
"""

import os
import re
import math
from ftplib import FTP
from datetime import datetime, timedelta
import logging

import lrpy as lp

logging.basicConfig(level=logging.INFO)


def list_cpf_filenames(target_name, date_str=None, ephemeris_source=None):
    """
    Return a list of cpf file names (absolute path).

    Parameters
    ----------
    target_name : str
        the official name of satellite, such as 'ajisai', 'lageos1', etc.
        It can be official short name, such as 'aji' for 'ajisai',
        'lg1' for 'lageos1', etc. see lp.satellite.

    date_str : str
        string of date in format '%y%m%d', such as '181024' for Oct 24 2018.
        If it is None, today is used.

    ephemeris_source : str
        ephemeris source, such as 'sgf', 'hts', 'utx', 'jax', etc.
        If it is None, all source is matched.

    Returns
    -------
         a list containing matched files' absolute path.
    """
    if len(target_name) <= 4:
        # it is target ext, such as 'aji', '128g'
        satellite = lp.satellite.satellite_by_ext(target_name)

        if satellite is not None:
            target_name = satellite['satellite_name']
        else:
            print('!!!!Error{}'.format(target_name))
            return []

    if date_str is not None:
        year = int(date_str[0:2]) + 2000
    else:
        date = datetime.utcnow()
        year = date.year
        date_str = date.strftime('%y%m%d')

    if ephemeris_source is None:
        ephemeris_source = '*'
    else:
        # lower case
        ephemeris_source = ephemeris_source.lower()

    remote_cpfdir = '{}/{}'.format(lp.remote_rootdir, lp.relative_cpfdir)
    local_cpfdir = '{}/{}'.format(lp.local_rootdir, lp.relative_cpfdir)

    # construct dir and filename pattern
    rel_dir = '{}/{}'.format(str(year), target_name)
    filename_pattern = '{}_cpf_{}_*.{}'.format(
            target_name, date_str, ephemeris_source)

    cpf_filenames = []

    # check if the file exist locally, if not download from FTP
    local_dir = '{}/{}'.format(local_cpfdir, rel_dir)
    if os.path.exists(local_dir):
        filenames = os.listdir(local_dir)
        for filename in filenames:
            if re.match(filename_pattern, filename):
                cpf_filenames.append('{}/{}'.format(local_dir, filename))
    else:
        os.makedirs(local_dir, exist_ok=True)

    # lp.ftp_server=none for offline
    if not cpf_filenames and lp.ftp_server:
        # download from ftp server
        logging.info('downloading cpf files from ftp for \'{}\' ... '.format(
                filename_pattern))

        with FTP(lp.ftp_server, user='anonymous') as ftp:
            wd = '{}/{}'.format(remote_cpfdir, rel_dir)
            ftp.cwd(wd)
            filenames = ftp.nlst(filename_pattern)

            if len(filenames) >= 1:
                # just download the first one
                remote_filename = filenames[0]
                local_filename = '{}/{}/{}'.format(
                        local_cpfdir, rel_dir, filenames[0])
                logging.info('RETR ' + local_filename)
                ftp.retrbinary('RETR '+remote_filename,
                               open(local_filename, 'wb').write)

                # add to list
                cpf_filenames.append(local_filename)

            ftp.quit()

        if not cpf_filenames:
            # no suitable cpf file, try the day-before
            new_date = datetime.strptime(date_str, '%y%m%d') \
                - timedelta(days=1)
            new_date_str = new_date.strftime('%y%m%d')

            logging.info('!!no suitable cpf file, try the day-before.')
            logging.info('new_date_str: {}'.format(new_date_str))

            return list_cpf_filenames(
                    target_name, new_date_str, ephemeris_source)

    return cpf_filenames


def cpf_absolute_filename(filename):
    """
    Return the absolute path for a given cpf filename.

    The filename is in the format:
        '{target_name}_cpf_{date_str}_{}.{ephemeris_source}'
    such as 'ajisai_cpf_181030_8031.jax', 'lageos1_cpf_181030_8031.jax',
    'beaconc_cpf_181030_8031.sgf', 'compassi3_cpf_181029_8021.sha'.
    """
    [target_name, dum, date_str, dum] = re.split(r'_', filename)

    local_cpfdir = '{}/{}'.format(lp.local_rootdir, lp.relative_cpfdir)

    year = int(date_str[0:2]) + 2000
    rel_dir = '{}/{}'.format(str(year), target_name)
    abs_filename = '{}/{}/{}'.format(local_cpfdir, rel_dir, filename)

    return abs_filename


_as_to_rad_ = math.radians(1.0 / 3600.0)


def npe(slant, tg_div, tg_reflect, tg_area,
        energy, wavelength, freq, div,
        rcv_area, opt_eff_trt, opt_eff_rcv,
        rcv_qe,
        atm_tran=0.6, atm_loss=0.1, debug=False):

    if tg_div == -1:
        tg_div = 648000  # 2*pi

    # div: as --> rad
    tg_div *= _as_to_rad_
    div *= _as_to_rad_

    # wavelength: nm --> m
    wavelength *= 1e-9

    # energy: mJ --> J
    energy *= 1e-3

    # photons
    E0 = energy
    N0 = E0 * wavelength / (lp.constants.h*lp.constants.c)
    N1 = N0 * opt_eff_trt
    r1 = slant * div / 2
    N2 = N1 * atm_loss * atm_tran * \
        tg_area / (math.pi * r1 * r1)
    N3 = N2 * tg_reflect
    r2 = slant * tg_div / 2
    N4 = N3 * atm_tran * rcv_area / (math.pi * r2 * r2)
    N5 = N4 * opt_eff_rcv

    # photoelectrons
    N6 = N5 * rcv_qe

    # probability, Poisson dist
    P = 1 - math.exp(-N6)

    rRate = P * freq

    if debug:
        print('N0~N5: {} {} {} {} {} {}'.format(N0, N1, N2, N3, N4, N5))
        print('N6={}'.format(N6))
        print('P={:.2f}%'.format(P*100))

    return (N6, P, rRate)
