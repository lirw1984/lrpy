#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
3d math, Vector3d, Matrix33

Classes:
    Vector3d
    A 3-dimensional vector, (x, y, z).

    Matrix33
    A 3 by 3 matrix, used for 3d transformation.

@author: lirw (lirw@ynao.ac.cn)
"""

import math


class Vector3d():
    """A 3-dimensional vector, (x, y, z)."""
    __slots__ = ('x', 'y', 'z')

    def __init__(self, x=0, y=0, z=0):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)

    def __eq__(self, oth):
        """
        Verify that two vectors are equal..

        Parameters
        ----------
        oth : Vector3d
            other vector to be verified.

        Returns
        -------
        true if xyz are 'equal' respectively, using math.isclose to check.
        if oth is not a Vector3d, return false.
        """
        if type(oth) is Vector3d:
            # using math.isclose not ==
            # default: rel_tol=1e-09, abs_tol=0.0
            return math.isclose(self.x, oth.x) and \
                    math.isclose(self.y, oth.y) and \
                    math.isclose(self.z, oth.z)
        else:
            return False

    def __str__(self):
        """
        Vector string for printing.

        Returns
        -------
        string to represent the vector
        """
        return self.__repr__()

    def __repr__(self):
        """
        repr for output

        Returns
        -------
        string to represent the vector
        """
        if type(self) is Vector3d:
            return 'Vector3d({}, {}, {})'.format(self.x, self.y, self.z)
        else:
            return NotImplemented

    def __add__(self, oth):
        """
        Override for operator '+'.

        Representing the vector add if 'oth' is a Vector3d,
        or a added vector for x, y, z respectively, if 'oth'
        is a number (float or int)

        Parameters
        ----------
        oth : Vector3d or number(float, int)
            a vector to add, or a number to be add.

        Returns
        -------
        a Vector3d

        Examples
        ----------
        >>> Vector3d(1, 2, 3) + Vector3d(4, 5, 6)
        Vector3d(5.0, 7.0, 9.0)
        >>> Vector3d(1, 2, 3) + 2
        Vector3d(3.0, 4.0, 5.0)
        """
        if type(oth) is Vector3d:
            # Vector substraction
            return Vector3d(self.x + oth.x, self.y + oth.y, self.z + oth.z)
        elif type(oth) in (float, int):
            # substract elements respectively
            return Vector3d(self.x + oth, self.y + oth, self.z + oth)
        else:
            return NotImplemented

    def __sub__(self, oth):
        """
        Override for operator '-'.

        Representing the vector subtraction if 'oth' is a Vector3d,
        or a substracted vector for x, y, z respectively, if 'oth'
        is a number (float or int)

        Parameters
        ----------
        oth : Vector3d or number(float, int)
            a vector to subtract, or a number to subtract.

        Returns
        -------
        a Vector3d

        Examples
        ----------
        >>> Vector3d(1, 2, 3) - Vector3d(4, 5, 6)
        Vector3d(-3.0, -3.0, -3.0)
        >>> Vector3d(1, 2, 3) - 2
        Vector3d(-1.0, 0.0, 1.0)
        """
        if type(oth) is Vector3d:
            # Vector substraction
            return Vector3d(self.x - oth.x, self.y - oth.y, self.z - oth.z)
        elif type(oth) in (float, int):
            # substract elements respectively
            return Vector3d(self.x - oth, self.y - oth, self.z - oth)
        else:
            return NotImplemented

    def __mul__(self, oth):
        """
        Override for operator '*'.

        A scalar value representing the dot product if 'oth' is a Vector3d,
        or a scaled vector if 'oth' is a number (float or int),
        otherwise NotImplemented.

        Parameters
        ----------
        oth : Vector3d or number(float, int)
            a vector for dot product, or a number to multiplication.

        Returns
        -------
        A scalar value if oth is a Vector3d,
        or a scaled vector if 'oth' is a number

        Examples
        ----------
        >>> Vector3d(1, 2, 3) * Vector3d(4, 5, 6)
        32.0
        >>> Vector3d(1, 2, 3) * 2
        Vector3d(2.0, 4.0, 6.0)
        """
        if type(oth) is Vector3d:
            # Scalar product of two vectors --> scalar
            return self.x * oth.x + self.y * oth.y + self.z * oth.z
        elif type(oth) in (float, int):
            # Multiplication of vector by scalar --> vector
            return Vector3d(self.x * oth, self.y * oth, self.z * oth)
        else:
            return NotImplemented

    def __rmul__(self, oth):
        """
        Override for operator right '*'.

        Return a scaled Vector3d.

        Note: Only for 'oth' is a number (float or int)

        Parameters
        ----------
        oth : number(float, int)
            a number to right multiplication.

        Returns
        -------
        a scaled vector

        Examples
        ----------
        >>> 2 * Vector3d(1, 2, 3)
        Vector3d(2.0, 4.0, 6.0)
        """
        if type(oth) in (float, int):
            return Vector3d(self.x * oth, self.y * oth, self.z * oth)
        else:
            return NotImplemented

    def __pow__(self, oth):
        """
        Override for operator '**'.

        Representing the cross product if 'oth' is a Vector3d,
        or a powed vector for x, y, z respectively, if 'oth'
        is a number (float or int)

        Parameters
        ----------
        oth : Vector3d or number(float, int)
            a vector for cross product, or a number to pow.

        Returns
        -------
        a Vector3d representing the cross product if oth is a Vector3d,
        or a powed vector if 'oth' is a number

        Examples
        ----------
        >>> Vector3d(1, 2, 3) ** Vector3d(4, 5, 6)
        Vector3d(-3.0, 6.0, -3.0)
        >>> Vector3d(1, 2, 3) **2.0
        Vector3d(1.0, 4.0, 9.0)
        """
        if type(oth) is Vector3d:
            # cross product
            return Vector3d(self.y * oth.z - self.z * oth.y,
                            self.z * oth.x - self.x * oth.z,
                            self.x * oth.y - self.y * oth.x)
        elif type(oth) in (float, int):
            # pow elements respectively
            return Vector3d(self.x ** oth, self.y ** oth, self.z ** oth)
        else:
            return NotImplemented

    @property
    def length(self):
        """
        Return the scalar length (Euclidean distance) of this vector.

        Returns
        -------
        the Euclidean distance of this vector

        Examples
        ----------
        >>> Vector3d(1, 2, 3).length
        3.7416573867739413
        """
        return math.hypot(math.hypot(self.x, self.y), self.z)

    @property
    def normalize(self):
        """
        A unit vector (length== 1 ) with the same angle as this one.

        Returns
        -------
        a unit vector

        Examples
        ----------
        >>> Vector3d(1, 2, 3).normalize
        Vector3d(0.2672612419124244, 0.5345224838248488, 0.8017837257372732)
        """
        d = self.length
        return Vector3d(self.x / d, self.y / d, self.z / d)

    def angular(self, oth):
        """
        The angle in radians between these two vectors.

        Parameters
        ----------
        oth : Vector3d
            a vector

        Returns
        -------
        the angle in radians

        Examples
        ----------
        >>> Vector3d(1, 2, 3).angular(Vector3d(4, 5, 6))
        0.2257261285527342
        """
        if type(oth) is Vector3d:
            cos_ang = (self * oth) / (self.length * oth.length)
            ang = math.acos(cos_ang)

            return ang
        else:
            return NotImplemented


class Matrix33:
    """
    A 3 by 3 matrix, used for 3d transformation.
    """
    def __init__(self, rows):
        self._rows = rows

        c1 = Vector3d(rows[0].x, rows[1].x, rows[2].x)
        c2 = Vector3d(rows[0].y, rows[1].y, rows[2].y)
        c3 = Vector3d(rows[0].z, rows[1].z, rows[2].z)

        self._cols = (c1, c2, c3)

    @classmethod
    def zero(cls):
        """Return an zero Matrix33."""
        rows = (Vector3d(0, 0, 0),
                Vector3d(0, 0, 0),
                Vector3d(0, 0, 0))

        cls(rows)

    @classmethod
    def one(cls):
        """Return an identity Matrix33."""
        rows = (Vector3d(1, 0, 0),
                Vector3d(0, 1, 0),
                Vector3d(0, 0, 1))

        cls(rows)

    @classmethod
    def rotation(cls, axis, angle):
        """"""
        # ii = axis - 1
        s = math.sin(angle)
        c = math.cos(angle)
        '''
        m = (Vector3d( c,  s, -s),
             Vector3d(-s,  c,  s),
             Vector3d( s, -s,  c))
        '''

        if axis == 1:
            rows = (Vector3d(1,  0,  0),
                    Vector3d(0,  c,  s),
                    Vector3d(0, -s,  c))
        elif axis == 2:
            rows = (Vector3d(c,  0, -s),
                    Vector3d(0,  1,  0),
                    Vector3d(s, -0,  c))
        else:
            rows = (Vector3d(+c,  s,  0),
                    Vector3d(-s,  c,  0),
                    Vector3d(+0,  0,  1))

        return cls(rows)

    def __repr__(self):
        if type(self) is Matrix33:
            return 'Matrix33({}, {}, {};\n{}, {}, {};\n{}, {}, {})'.format(
                self._rows[0].x, self._rows[0].y, self._rows[0].z,
                self._rows[1].x, self._rows[1].y, self._rows[1].z,
                self._rows[2].x, self._rows[2].y, self._rows[2].z)
        else:
            return NotImplemented

    def __mul__(self, other):
        """
        Override for operator '*'.

        A Vector3d representing the dot product if 'oth' is a Vector3d,
        or a scaled Matrix33 if 'oth' is a number (float or int),
        otherwise NotImplemented.

        Parameters
        ----------
        oth : Vector3d or number(float, int)
            a Vector3d for dot product, or a number to multiplication.

        Returns
        -------
        A Vector3d value if oth is a Vector3d,
        or a scaled Matrix33 if 'oth' is a number
        """
        if type(other) is Vector3d:
            # Scalar product of two vectors --> scalar
            return Vector3d(self._rows[0] * other, self._rows[1] * other,
                            self._rows[2] * other)
        elif type(other) in (float, int):
            ##
            return other * self
        else:
            return NotImplemented

    def __rmul__(self, other):
        """
        Override for operator right '*'.

        Return a scaled Matrix33.

        Note: Only for 'oth' is a number (float or int)

        Parameters
        ----------
        oth : number(float, int)
            a number to right multiplication.

        Returns
        -------
        a scaled Matrix33
        """
        if type(other) in (float, int):
            rows = (other * self._rows[0], other * self._rows[1],
                    other * self._rows[2])
            return Matrix33(rows)
        else:
            return NotImplemented

    def __pow__(self, other):
        """[ ** ] Vector3d product self x other"""
        if type(other) is Matrix33:
            r1 = Vector3d(self._rows[0] * other._cols[0],
                          self._rows[0] * other._cols[1],
                          self._rows[0] * other._cols[2])
            r2 = Vector3d(self._rows[1] * other._cols[0],
                          self._rows[1] * other._cols[1],
                          self._rows[1] * other._cols[2])
            r3 = Vector3d(self._rows[2] * other._cols[0],
                          self._rows[2] * other._cols[1],
                          self._rows[2] * other._cols[2])
            return Matrix33((r1, r2, r3))
        else:
            return NotImplemented

    @property
    def rows(self):
        """rows"""
        return self._rows

    @property
    def cols(self):
        """cols"""
        return self._cols

    @property
    def T(self):
        """transpose"""
        return Matrix33(self._cols)
