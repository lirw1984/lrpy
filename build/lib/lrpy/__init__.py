#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
LrPy (Laser Ranging Python)

It is implemention cpf and crd algorithm of ilrs in python. According to
the original ilrs sample code, for cpf, it mainly implements readin, files
managing, check, interpolating for sched/slr/llr, and for crd, it mainly
implements readin, write, check, split, merg, conversion between crd and
older SLR/LLR formats.


**Key Features**

- auto download cpf file from ftp if local does not exist
- query satellite by id, name, norad, ext
- reading and interpolating of cpf file
- pass finding in seconds resolution (sched)
- generate tracking file (pred)
- configure based
- custom trackdata formatter

**Example Usage**

The using of lrpy is simple. The following is generated at 2018-10-25.

>>>import lrpy as lp

>>>cpf_filenames = lp.util.list_cpf_filenames('ajisai')
INFO:root:downloading cpf files from ftp for 'ajisai_cpf_181025_*.*' ...
INFO:root:RETR ./cpf_predicts/2018/ajisai/ajisai_cpf_181025_7981.jax

>>>cpf = lp.cpf.CPF(cpf_filenames[0])
>>>passes = lp.sched(cpf)
INFO:root:do sched with (2018-10-25T00:00:00~2018-10-25T23:00:00) for
ajisai(ajisai_cpf_181025_7981.hts)

>>>passes
[20181025 7819    aji 054334 055434 32 054850 ajisai_cpf_181025_7981.hts,
 20181025 7819    aji 074412 075811 73 075112 ajisai_cpf_181025_7981.hts,
 20181025 7819    aji 203515 204156 23 203836 ajisai_cpf_181025_7981.hts,
 20181025 7819    aji 223242 224649 77 223944 ajisai_cpf_181025_7981.hts]

>>>npass = passes[0]
>>>start_datetime = npass.open_trackdata.epoch
>>>end_datetime = npass.close_trackdata.epoch
>>>track_filename = '{}.{}'.format(start_datetime.strftime('%Y%m%dT%H%M%S'),
                               lp.satellite_by_name('ajisai')['ext'])
>>>trackdatas = lp.predict_all(cpf, lp.default_station,
                               start_datetime, end_datetime,
                               interval='1s', filename=track_filename,
                               formatter=lp.core.default_trackdata_formatter)
INFO:root:generate tracking file within (2018-10-25T05:43:34~
2018-10-25T05:54:34, 1s) for ajisai(ajisai_cpf_181025_7981.jax)
INFO:root:output to the file: 20181025T054334.aji

**Specifications**

- Modules

satellite  ILRS satellites manager
station    Station related, using WGS-84 system
cpf        Consolidated Laser Ranging Prediction Format
core       Core lib for cpf algorithm (both slr and llr)
util       Util for file/path handling
constants  Constants related
lagrange   lagrange interpolation
math3d     3d math, Vector3d, Matrix33

- Key Interfaces
    list_cpf_filenames      Return a list of cpf file names (absolute path)
    cpf_absolute_filename   Return the absolute path for a given cpf filename

    clkcor          Return clock correction for the relativistic differential
                    relationship
    mjd_sod         Return mjd and sod for a given UTC datetime
    sched           Find pass schedule for the given cpf and station.
    predict_all     Generate tracking data for the given cpf and station from
                    start_datetime to end_datetime.
    predict         Compute a TrackData for slr at given datetime.
    predict_llr     Compute a TrackData for llr at given datetime.


    default_trackdata_formatter     Return a formatted string for trackdata
                                    to generate a tracking file

    satellite_by_id     Return a satellite by given id, None if not valid
    satellite_by_name   Return a satellite by given official name,
                        None if not valid
    satellite_by_norad  Return a satellite by given norad number,
                        None if not valid
    satellite_by_ext    Return a satellite by given official extention,
                        None if not valid

**Tests**

According to the original ilrs sample code, it passed sched and pred test.

- sched:

gps36_cpf_051129_0083.cod, MLRS(255.98481, 30.68022, 2006.22100, 7080)
(2005-12-01T00:00:00~2005-12-04T23:00:00)

20051201 7080    g36 001635 052212 82 030020 gps36_cpf_051129_0083.cod
20051202 7080    g36 001227 051805 82 025612 gps36_cpf_051129_0083.cod
20051203 7080    g36 000819 051358 82 025205 gps36_cpf_051129_0083.cod
20051204 7080    g36 000410 050950 82 024757 gps36_cpf_051129_0083.cod

- pred_slr:

gps36_cpf_051129_0083.cod, (4331283.557, 567549.902, 4633140.353),
(2005-11-30T01:12:00~2005-12-01T00:00:00, 1s)

diff gps36_cpf_051129_0083_cod.op gps36_cpf_051129_0083_cod.op.ref.python

- pred_llr:

apollo15_cpf_061220_8551.utx, MLRS(255.98481, 30.68022, 2006.22100, 7080)
(2006-12-20T14:15:00~2006-12-20T22:15:00, 15m)

diff apollo15_cpf_061220_y061220t1415 apollo15_cpf_061220_y061220t1415.ref.python

@author: lirw (lirw@ynao.ac.cn)
"""
import lrpy.constants
import lrpy.util

import lrpy.cpf
from lrpy.station import Station
from lrpy.math3d import Vector3d, Matrix33
from lrpy.core import sched, predict, predict_llr, predict_all, mjd_sod, \
                     default_trackdata_formatter
from lrpy.satellite import satellite_by_id, satellite_by_name, \
                            satellite_by_norad, satellite_by_ext

__version__ = '1.0'

local_rootdir = '.'

ftp_server = 'edc.dgfi.tum.de'
remote_rootdir = '/pub/slr'
relative_cpfdir = 'cpf_predicts'
relative_nptcrddir = 'data/npt_crd'

# default is KUNMING2 (7819, 102.7977E, 25.0298N, 1987.05m)
default_station = Station(1.7941583284, 0.4368524211, 1987.050,
                          'KUNMING2', 7819)

openclose_elevation = 19
valid_pass_highest_elevation = 0
start_hours = 0.0
end_hours = 23.0


def load_config(filenames='config.ini'):
    """
    load config for default station and pred related params and return config.

    Parameters:
        filenames    list or str    a config filename list
    Parameters
    ----------
    filenames: str or list
        a config filename or a list filenames

    Returns
    ----------
    a config instance.
    """
    global default_station, \
        ftp_server, remote_rootdir, relative_cpfdir, relative_nptcrddir, \
        local_rootdir, \
        openclose_elevation, valid_pass_highest_elevation,\
        start_hours, end_hours

    import configparser
    import math

    config = configparser.ConfigParser()
    # Read and parse a filename or a list of filenames.
    # Return list of successfully read files.
    config.read(filenames)

    # params for station
    longitude = float(config['station']['longitude'])
    latitude = float(config['station']['latitude'])
    height = float(config['station']['height'])
    name = config['station']['name']
    station_id = int(config['station']['id'])

    default_station = Station(math.radians(longitude),
                              math.radians(latitude),
                              height,
                              name, station_id)
    # params for paths
    ftp_server = config['paths']['ftp_server']
    remote_rootdir = config['paths']['remote_rootdir']
    local_rootdir = config['paths']['local_rootdir']
    relative_cpfdir = config['paths']['relative_cpfdir']
    relative_nptcrddir = config['paths']['relative_nptcrddir']

    # params for sched/pred
    openclose_elevation = float(config['pred']['openclose_elevation'])
    valid_pass_highest_elevation = \
        float(config['pred']['valid_pass_highest_elevation'])
    start_hours = float(config['pred']['start_hours'])
    end_hours = float(config['pred']['end_hours'])

    return config
