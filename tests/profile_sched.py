#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import cProfile
import math

from datetime import datetime

import lrpy as lp


def profile_sched():
    station = lp.Station(math.radians(102.7974337621),
                         math.radians(25.029965376),
                         1993.0114277305, '1m2', 7820)

    #f = lp.cpf.CPF('lageos2_cpf_180205_5361.hts')
    #start_datetime = datetime(2018, 2, 6, 11)
    #end_datetime = datetime(2018, 2, 6, 23)

    f = lp.cpf.CPF('gps36_cpf_051129_0083.cod')

    start_datetime = datetime(2005, 12, 1)
    end_datetime = datetime(2005, 12, 4, 23)

    pass_list = lp.sched(f, station, start_datetime, end_datetime)
    for npass in pass_list:
        print(npass)
    pass


if __name__ == "__main__":
    cProfile.run("profile_sched()")
