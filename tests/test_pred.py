#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import math
from datetime import datetime

import unittest

import lrpy as lp


def llr_trackdata_formatter(trackdata, pre_trackdata=None):
    (mjd, sod) = lp.mjd_sod(trackdata.epoch)
    outstr = \
        'Time: {} {}\n{:9.4f} {:9.4f} {:14.11f}\n{:9.4f} {:9.4f} {:14.11f}\n' \
        .format(trackdata.mjd, trackdata.epoch,
                math.degrees(trackdata.azimuth_out),
                math.degrees(trackdata.elevation_out),
                trackdata.flight_time,
                math.degrees(trackdata.azimuth_in),
                math.degrees(trackdata.elevation_in),
                trackdata.flight_time
                )

    return outstr


class PredTest(unittest.TestCase):
    def test_slr(self):
        station = lp.Station.from_position(
                lp.Vector3d(4331283.557, 567549.902, 4633140.353))
        print(repr(station))

        f = lp.cpf.CPF('gps36_cpf_051129_0083.cod')

        start_datetime = datetime(2005, 11, 30, 1, 12)
        end_datetime = datetime(2005, 12, 1, 0)

        trackdatas = lp.predict_all(f, station, start_datetime, end_datetime,
                                    min_elevation=-180,
                                    filename='gps36_cpf_051129_0083_cod.op')

        cmd = 'diff gps36_cpf_051129_0083_cod.op gps36_cpf_051129_0083_cod.op.ref.python'
        ret = os.system(cmd)
        self.assertEqual(ret, 0)

    def test_llr(self):
        station = lp.Station(math.radians(255.9848038),
                             math.radians(30.6802667),
                             2006.0880000, 'MLRS', 7080)
        print(repr(station))

        f = lp.cpf.CPF('apollo15_cpf_061220_8551.utx')

        start_datetime = datetime(2006, 12, 20, 14, 15)
        end_datetime = datetime(2006, 12, 20, 22, 15)

        trackdatas = lp.predict_all(f, station, start_datetime, end_datetime,
                                    min_elevation=-180, interval='15m',
                                    formatter=llr_trackdata_formatter,
                                    filename='apollo15_cpf_061220_y061220t1415')

        cmd = 'diff apollo15_cpf_061220_y061220t1415 apollo15_cpf_061220_y061220t1415.ref.python'
        ret = os.system(cmd)
        self.assertEqual(ret, 0)


if __name__ == '__main__':
    unittest.main()
