#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from datetime import datetime
import math

import unittest

import lrpy as lp


class SchedTest(unittest.TestCase):
    def test_gps36(self):
        station = lp.Station(math.radians(255.98481), math.radians(30.68022),
                             2006.22100, 'MLRS01', 7080)
        print(repr(station))

        f = lp.cpf.CPF('gps36_cpf_051129_0083.cod')

        start_datetime = datetime(2005, 12, 1)
        end_datetime = datetime(2005, 12, 4, 23)

        pass_list = lp.sched(f, station, start_datetime, end_datetime,
                             openclose_elevation=19)
        for npass in pass_list:
            print(npass)

        # 20051201 7080 g36 001635 052212  82 030019 ************* gps36_cpf_051129_0083.cod
        # 20051202 7080 g36 001227 051805  82 025612 ************* gps36_cpf_051129_0083.cod
        # 20051203 7080 g36 000819 051358  82 025205 ************* gps36_cpf_051129_0083.cod
        # 20051204 7080 g36 000410 050950  82 024757 ************* gps36_cpf_051129_0083.cod
        self.assertEqual(4, len(pass_list))
        self.assertEqual(
            '20051201 7080    g36 001635 052212 82 030020 gps36_cpf_051129_0083.cod',
            repr(pass_list[0]))
        self.assertEqual(
            '20051204 7080    g36 000410 050950 82 024757 gps36_cpf_051129_0083.cod',
            repr(pass_list[-1]))
        self.assertEqual(
            '20051203 7080    g36 000819 051358 82 025205 gps36_cpf_051129_0083.cod',
            repr(pass_list[2]))


if __name__ == '__main__':
    unittest.main()
