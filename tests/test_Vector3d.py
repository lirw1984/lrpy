#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 23 15:21:10 2018

@author: lirw
"""

import unittest

from lrpy import Vector3d


class Vector3dTest(unittest.TestCase):
    def test_add_vector(self):
        v = Vector3d(1, 2, 3) + Vector3d(4, 5, 6)

        self.assertEqual(v, Vector3d(5.0, 7.0, 9.0))

    def test_add_number(self):
        v = Vector3d(1, 2, 3) + 2

        self.assertEqual(v, Vector3d(3.0, 4.0, 5.0))

    def test_sub_vector(self):
        v = Vector3d(1, 2, 3) - Vector3d(4, 5, 6)

        self.assertEqual(v, Vector3d(-3.0, -3.0, -3.0))

    def test_sub_number(self):
        v = Vector3d(1, 2, 3) - 2

        self.assertEqual(v, Vector3d(-1.0, 0.0, 1.0))

    def test_mul_vector(self):
        s = Vector3d(1, 2, 3) * Vector3d(4, 5, 6)

        self.assertEqual(s, 32.0)

    def test_mul_number(self):
        v = Vector3d(1, 2, 3) * 2

        self.assertEqual(v, Vector3d(2.0, 4.0, 6.0))

    def test_rmul_number(self):
        v = 2 * Vector3d(1, 2, 3)

        self.assertEqual(v, Vector3d(2.0, 4.0, 6.0))

    def test_pow_vector(self):
        v = Vector3d(1, 2, 3) ** Vector3d(4, 5, 6)

        self.assertEqual(v, Vector3d(-3.0, 6.0, -3.0))

    def test_pow_number(self):
        v = Vector3d(1, 2, 3) ** 2

        self.assertEqual(v, Vector3d(1.0, 4.0, 9.0))

    def test_length(self):
        s = Vector3d(1, 2, 3).length

        self.assertEqual(s, 3.7416573867739413)

    def test_normalize(self):
        v = Vector3d(1, 2, 3).normalize

        self.assertEqual(v,
                         Vector3d(0.2672612419124244,
                                  0.5345224838248488,
                                  0.8017837257372732))

    def test_angular(self):
        s = Vector3d(1, 2, 3).angular(Vector3d(4, 5, 6))

        self.assertEqual(s, 0.2257261285527342)


if __name__ == '__main__':
    unittest.main()
