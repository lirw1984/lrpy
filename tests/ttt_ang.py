#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 14:56:58 2018

@author: lirw
"""

import math
from datetime import datetime, timedelta

import unittest

import lrpy as lp


class PredTest(unittest.TestCase):
    def test_llr(self):
        station = lp.Station(math.radians(255.9848038),
                             math.radians(30.6802667),
                             2006.0880000, 'MLRS', 7080)
        print(repr(station))

        f = lp.cpf.CPF('apollo15_cpf_061220_8551.utx')

        start_datetime = datetime(2006, 12, 20, 14, 15)
        end_datetime = datetime(2006, 12, 20, 22, 15)

        stepT = timedelta(minutes=15)
        dt = start_datetime

        while dt <= end_datetime:
            tr0 = lp.predict_llr(f, station, dt, mode=1)
            print('transmit(t0)   : {}'.format(tr0))
            print('recieve(t0+dt) : {:>11} {:11.5f} {:11.5f}'.format(
                    '',
                    math.degrees(tr0.azimuth_in),
                    math.degrees(tr0.elevation_in)))

            tof = tr0.flight_time*1e3
            rcv_dt = dt + timedelta(milliseconds=tof)
            tr2 = lp.predict_llr(f, station, rcv_dt, mode=1)
            print('transmit(t0+dt): {}'.format(tr2))

            dA = math.degrees(tr2.azimuth_out - tr0.azimuth_in) * 3600
            dE = math.degrees(tr2.elevation_out - tr0.elevation_in) * 3600
            cosE = math.cos(tr2.elevation_out)
            dR = math.sqrt(dA * cosE * dA * cosE + dE * dE)

            print('dA={:.2f} dE={:.2f}, dR={:.2f}'.format(dA, dE, dR))

            print()

            dt = dt + stepT


if __name__ == '__main__':
    unittest.main()
