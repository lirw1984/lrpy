LrPy (Laser Ranging Python)

It is implemention cpf and crd algorithm of ilrs in python. According to the original ilrs sample code, for cpf, it mainly implements readin, files managing, check, interpolating for sched/slr/llr, and for crd, it mainly implements readin, write, check, split, merg, conversion between crd and older SLR/LLR formats.


**Key Features**

- auto download cpf file from ftp if local does not exist
- query satellite by id, name, norad, ext
- reading and interpolating of cpf file
- pass finding in seconds resolution (sched)
- generate tracking file (pred)
- configure based
- custom trackdata formatter

**Example Usage**

The using of lrpy is simple.

>>>import lrpy as lp

>>>cpf_filenames = lp.util.list_cpf_filenames('ajisai')
INFO:root:downloading cpf files from ftp for 'ajisai_cpf_181025_*.*' ...
INFO:root:RETR ./cpf_predicts/2018/ajisai/ajisai_cpf_181025_7981.jax

>>>cpf = lp.cpf.CPF(cpf_filenames[0])
>>>passes = lp.sched(cpf)
INFO:root:do sched with (2018-10-25T00:00:00~2018-10-25T23:00:00) for ajisai(ajisai_cpf_181025_7981.hts)

>>>passes
[20181025 7819    aji 054334 055434 32 054850 ajisai_cpf_181025_7981.hts,
 20181025 7819    aji 074412 075811 73 075112 ajisai_cpf_181025_7981.hts,
 20181025 7819    aji 203515 204156 23 203836 ajisai_cpf_181025_7981.hts,
 20181025 7819    aji 223242 224649 77 223944 ajisai_cpf_181025_7981.hts]

>>>from datetime import datetime
>>>start_datetime = datetime.strptime('20181025054334', '%Y%m%d%H%M%S')
>>>end_datetime = datetime.strptime('20181025055434', '%Y%m%d%H%M%S')
>>>trackdatas = lp.predict_all(cpf, lp.default_station,
                                start_datetime, end_datetime,
                                interval='1s',
                                filename='20181025T054334.aji',
                                formatter=lp.core.default_trackdata_formatter)

**Specifications**

- Modules

satellite  ILRS satellites manager
station    Station related, using WGS-84 system
cpf        Consolidated Laser Ranging Prediction Format
core       Core lib for cpf algorithm (both slr and llr), including sched, pred, etc
util       Util for list cpf filenames, the absolute path for a given cpf filename, etc
constants  Constants related
lagrange   lagrange interpolation
math3d     3d math, Vector3d, Matrix33

- Key Interfaces
    list_cpf_filenames      Return a list of cpf file names (absolute path)
    cpf_absolute_filename   Return the absolute path for a given cpf filename

    clkcor          Return clock correction for the relativistic differential
                    relationship
    mjd_sod         Return mjd and sod for a given UTC datetime
    sched           Find pass schedule for the given cpf and station.
    predict_all     Generate tracking data for the given cpf and station from
                    start_datetime to end_datetime.
    predict         Compute a TrackData for slr at given datetime.
    predict_llr     Compute a TrackData for llr at given datetime.


    default_trackdata_formatter     Return a formatted string for trackdata
                                    to generate a tracking file

    satellite_by_id     Return a satellite by given id, None if not valid
    satellite_by_name   Return a satellite by given official name,
                        None if not valid
    satellite_by_norad  Return a satellite by given norad number,
                        None if not valid
    satellite_by_ext    Return a satellite by given official extention,
                        None if not valid

**Tests**

According to the original ilrs sample code, it passed sched and pred test.

- sched:

gps36_cpf_051129_0083.cod, MLRS(255.98481, 30.68022, 2006.22100, 7080)
(2005-12-01T00:00:00~2005-12-04T23:00:00)

20051201 7080    g36 001635 052212 82 030020 gps36_cpf_051129_0083.cod
20051202 7080    g36 001227 051805 82 025612 gps36_cpf_051129_0083.cod
20051203 7080    g36 000819 051358 82 025205 gps36_cpf_051129_0083.cod
20051204 7080    g36 000410 050950 82 024757 gps36_cpf_051129_0083.cod

- pred_slr:

gps36_cpf_051129_0083.cod, (4331283.557, 567549.902, 4633140.353),
(2005-11-30T01:12:00~2005-12-01T00:00:00, 1s)

diff gps36_cpf_051129_0083_cod.op gps36_cpf_051129_0083_cod.op.ref.python

- pred_llr:

apollo15_cpf_061220_8551.utx, MLRS(255.98481, 30.68022, 2006.22100, 7080)
(2006-12-20T14:15:00~2006-12-20T22:15:00, 15m)

diff apollo15_cpf_061220_y061220t1415 apollo15_cpf_061220_y061220t1415.ref.python


