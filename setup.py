#!/usr/bin/env python
# -*- coding: utf-8 -*-

from distutils.core import setup


setup(name='lrpy',
      version='1.0',
      description='Module for Laser Ranging, cpf and crd algorithm',
      long_description='',
      author='LI Rongwang',
      author_email='lirw@ynao.ac.cn',
      license='BSD',
      keywords=['laser ranging', 'cpf', 'crd'],
      url='https://www.python.org/',
      packages=['lrpy'],
      package_dir={'lrpy': 'src/lrpy'},
      package_data={'lrpy': ['data/*.json',
                             'lagrange.win_amd64-msvc10.dll',
                             'lagrange.x86_64-linux-gnu.so']},
      platforms='',
      classifiers=[
          'Development Status :: 4 - Beta',
          'Environment :: Console',
          'Intended Audience :: Developers ',
          'Intended Audience :: Science/Research',
          "License :: OSI Approved :: BSD License",
          'Operating System :: Microsoft :: Windows',
          'Operating System :: POSIX',
          'Programming Language :: Python',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 3.6',
          'Topic :: Scientific/Engineering :: Physics',
          'Topic :: Scientific/Engineering :: Astronomy',
          ]
      )
